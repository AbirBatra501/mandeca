import { TimelineComponentModule } from './timeline/timeline.module';
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { FileuploadComponent } from './fileupload/fileupload';



export const components = [

];

@NgModule({
  declarations: [components,
    FileuploadComponent],
  imports: [IonicModule],
  exports: [components, TimelineComponentModule,
    FileuploadComponent]
})
export class ComponentsModule {}
