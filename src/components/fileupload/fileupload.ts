import { Component } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ConfigProvider } from '../../providers/config';
import { publitioApi } from 'publitio_js_sdk';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { map } from 'rxjs/operators';
import * as _loadash from 'lodash';
import { AlertController } from 'ionic-angular';

var publitio = publitioApi('D9dPPvn4Pj6ne7s4TzaR', 'Jqld6aHZbSzLP846zTSVk0Af8e5rq9dp');


/**
 * Generated class for the FileuploadComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'fileupload',
  templateUrl: 'fileupload.html'
})
export class FileuploadComponent {




  uploadStatus: boolean = false;

  constructor(public alertCtrl: AlertController, public camera: Camera, public config: ConfigProvider, private httpClient: HttpClient) {
    console.log('Hello FileuploadComponent Component');
    
    this.config.uploadPhotosQoeue = [];
  }


  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,//this.platform.is('ios') ? this.camera.DestinationType.FILE_URI : this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = null;
      base64Image = "data:image/jpeg;base64," + imageData;
      var name = this.config.stringGen(10) + ".png";
      //Usage example:
      var file = this.dataURLtoFile(base64Image, name);
      this.showPrompt(name, file, base64Image);



    }, (error) => {
      console.debug("Unable to obtain picture: " + error, "app");
      console.log(error);
    });
  }

  showPrompt(name, file, base64Image) {
    const prompt = this.alertCtrl.create({
      title: 'Description',
      message: "Please add photo/video description",
      inputs: [
        {
          name: 'title',
          placeholder: 'Description'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            var newImageObject = { "name": name, file: file, "src": base64Image, desc: "", progress: 0, uploaded: false };
            this.config.uploadPhotosQoeue.push(newImageObject);
            this.uploadFileToServer();
          }
        },
        {
          text: 'Save',
          handler: data => {
            var newImageObject = { "name": name, file: file, "src": base64Image, desc: data.title, progress: 0, uploaded: false };
            this.config.uploadPhotosQoeue.push(newImageObject);
            this.uploadFileToServer();
          }
        }
      ]
    });
    prompt.present();
  }

  showPromptForEditDesc(p) {
    const prompt = this.alertCtrl.create({
      title: 'Description',
      message: "Please add photo/video description",
      inputs: [
        {
          name: 'title',
          placeholder: p.desc
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {

          }
        },
        {
          text: 'Save',
          handler: data => {
            if (data.title != "")
              p.desc = data.title;
          }
        }
      ]
    });
    prompt.present();
  }

  uploadFileToServer() {
    if (this.uploadStatus == false) {
      var imageObject = this.getNextImageToUpload();
      if (imageObject != "no_images_to_upload") {
        this.config.enablePageActions = false;
        this.uploadStatus = true;
        this.sendfileToServer(imageObject).subscribe(
          (res: any) => {

            if (res.progress)
              imageObject.progress = res.progress;
            else if (res.event) {
              if (res.event.status == 201) {
                imageObject.uploaded = true;
              }
              else {
                this.config.presentToastError(this.config.internetConnectionErrorMsg);
              }
              this.uploadStatus = false;
              this.uploadFileToServer();
            }

          },
          (err) => {
            this.uploadStatus = false;
            this.config.presentToastError(this.config.internetConnectionErrorMsg);
          }
        );
      } else {
        this.config.enablePageActions = true;
      }
    }
  }

  sendfileToServer(imageObject) {
    const formData = new FormData();
    formData.append('file', imageObject.file);
    var uploadURL = publitio.uploadUrlSigned("file", { folder: this.config.publitioUploadDirPath });
    return this.httpClient.post<any>(uploadURL, formData, {
      reportProgress: true,
      observe: 'events'
    }).pipe(map((event) => {
      switch (event.type) {
        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { progress: progress };
        case HttpEventType.Response:
          return { event: event };
        default:
          return `Unhandled event: ${event.type}`;
      }
    }));

  }




  getNextImageToUpload() {
    var imagesToBeUploaded = _loadash.filter(this.config.uploadPhotosQoeue, _loadash.matches({ 'uploaded': false }));
    if (imagesToBeUploaded.length > 0)
      return imagesToBeUploaded[0];
    else
      return "no_images_to_upload";
  }

  dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }

  editDescription(p) {
    this.showPromptForEditDesc(p);
  }

  deletePic(p) {
    const confirm = this.alertCtrl.create({
      title: 'Delete?',
      message: 'Do you confirm to delete?',
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: 'Ok',
          handler: () => {
            this.config.uploadPhotosQoeue.splice(this.config.uploadPhotosQoeue.indexOf(p.name), 1);
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present();
  }

}
