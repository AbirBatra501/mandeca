import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { StatusPage } from '../status/status';
import { ApprovePage } from '../approve/approve';
import { RequestWorkPage } from '../request-work/request-work';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { Events } from 'ionic-angular/util/events';

@Component({
  selector: 'page-tabs-controller',
  templateUrl: 'tabs-controller.html'
})
export class TabsControllerOwnerPage {

  tabindex: any = 0;
  
  tab1Root: any = StatusPage;
  tab2Root: any = RequestWorkPage;
  tab3Root: any = ApprovePage;
  
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
    
    this.tabindex = navParams.get('index');;
  }

  ionViewDidLoad() {
    this.events.publish('setusernameandmenu', 'All');
  }
 
}
