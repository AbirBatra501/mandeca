import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ConfigProvider } from '../../../providers/config';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-task-done',
  templateUrl: 'task-done.html'
})
export class TaskDoneOwnerPage {
  taskname: any = "";
  taskdescription: any = "";
  tphotosdata: any = ""
  taskid: any = 0;
  property_name: any = "";
  imageurl: any = "";

  setupdate: any = "";
  setuptime: any = "";

  userid: any = 0;
  createdby_id: any = 0;
  currentuserType: any;
  selectedpropid: any = 0;


  Photosarray: any = [];

  photosdata: any = [];
  descdata: any = [];
  taskphoto: any = "";

  deletepicarray: any = [];
  selectedpropname: any = "";

  assigned_group: any;
  assignedpersonname: any;
  assigned_person_id: any;


  propertyid: any = 0;
  userdata: any = [];
  user: any;

  taskstarttime: any = "";
  taskendtime: any = "";



  constructor(navParams: NavParams, public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider) {
    try {
      //alert(JSON.stringify(navParams));
      this.setupdate = navParams.get('setup_date');
      this.setuptime = navParams.get('setup_time');

      this.taskdescription = navParams.get('task_description');
      var taskdescSplit = this.taskdescription.split(" ");
      if (taskdescSplit.length > 2)
        this.taskname = taskdescSplit[0] + " " + taskdescSplit[1];
      else
        this.taskname = this.taskdescription;
      this.taskid = navParams.get('taskid');
      this.property_name = navParams.get('propname');
      var photos = navParams.get('photos');


      this.taskphoto = photos.split(",")[0];
      //this.photosdata = photos.split(",");
      this.assigned_group = navParams.get('assigned_group');
      this.assignedpersonname = navParams.get('assigned_person_name');
      this.assigned_person_id = navParams.get('assigned_person_id');
      this.propertyid = navParams.get('propid');
      this.imageurl = this.config.IMAGE_URL;
    }
    catch (e) {
      //alert(e);
    }
  }

  ionViewDidEnter() {
    
    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
      //this.getTaskPerformedData(this.taskid);
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });
    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.localStorageService.get("selectedpropid").then((val) => {
        if (val != "" && val != null)
          this.selectedpropid = val;
        this.GetTaskPerformedData(this.taskid);
        //this.SelectGroup(this.assigned_group);
      });
    });
    this.imageurl = this.config.IMAGE_URL;
  }




  GetTaskPerformedData(taskid) {
    this.userdata = [];
    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "taskperformed",
      task_id: taskid
    };

    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait...").then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = JSON.parse(ServerResponse.result);
          this.taskstarttime = a[0].start_time;
          this.taskendtime = a[0].end_time;
          var photos = a[0].photos;
          var photos_desc = a[0].photos_desc;

          if (photos != null && photos != "") {
            var pdata = photos.split(",");
            //alert(pdata.length);
            for (var k = 0; k < pdata.length; k++) {
              var desc = "";
              if (photos_desc != null && photos_desc != "") {
                this.descdata = photos_desc.split(",");
                desc = this.descdata[k];
              }

              //alert(pdata[k]);
              this.photosdata.push({ "name": pdata[k], "src": pdata[k], "desc": desc });
            }
          }
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }



}
