import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ConfigProvider } from '../../../providers/config';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-task-name',
  templateUrl: 'task-name.html'
})
export class TaskNameOwnerPage {
  taskname: any = "";
  taskdescription: any = "";
  taskid: any = 0;
  property_name: any = "";
  imageurl: any = "";

  setupdate: any = "";
  setuptime: any = "";

  userid: any = 0;
  createdby_id: any = 0;
  currentuserType: any;
  selectedpropid: any = 0;



  photosdata: any = [];
  descdata: any = [];
  taskphoto: any = "";

  deletepicarray: any = [];
  selectedpropname: any = "";

  assigned_group: any;
  assignedpersonname: any;
  assigned_person_id: any;

  owner_approval: any;
  approved_by: any;

  propertyid: any = 0;
  userdata: any = [];
  user: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public config: ConfigProvider, public localStorageService: Storage) {
    try {
      //alert(JSON.stringify(navParams));
      this.setupdate = navParams.get('setup_date');
      this.setuptime = navParams.get('setup_time');

      this.taskdescription = navParams.get('task_description');
      var taskdescSplit = this.taskdescription.split(" ");
      if (taskdescSplit.length > 2)
        this.taskname = taskdescSplit[0] + " " + taskdescSplit[1];
      else
        this.taskname = this.taskdescription;
      this.taskid = navParams.get('taskid');
      this.property_name = navParams.get('propname');
      var photos = navParams.get('photos');
      var photos_desc = navParams.get('desc');

      if (photos != null && photos != "") {
        var pdata = photos.split(",");
        //alert(pdata.length);
        for (var k = 0; k < pdata.length; k++) {
          var desc = "";
          if (photos_desc != null && photos_desc != "") {
            this.descdata = photos_desc.split(",");
            desc = this.descdata[k];
          }

          //alert(pdata[k]);
          this.photosdata.push({ "name": pdata[k], "src": pdata[k], "desc": desc });
        }
      }

      //this.taskphoto = photos.split(",")[0];
      //this.photosdata = photos.split(",");
      this.assigned_group = navParams.get('assigned_group');
      this.assignedpersonname = navParams.get('assigned_person_name');
      this.assigned_person_id = navParams.get('assigned_person_id');
      this.owner_approval = navParams.get('owner_approval');
      this.approved_by = navParams.get('approved_by');
      this.propertyid = navParams.get('propid');
      this.imageurl = this.config.IMAGE_URL;
    }
    catch (e) {

    }
  }

  ionViewDidEnter() {

    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
    });

    this.imageurl = this.config.IMAGE_URL;
  }

  ApproveTask() {
    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "ApproveTask",
      task_id: this.taskid,
      approved_by: this.userid
    };
    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.config.presentToastSuucess(ServerResponse.remarks);
          this.navCtrl.pop();
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

}
