import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../../../providers/config';
import { TaskDoneOwnerPage } from '../task-done/task-done';
import { Camera, CameraOptions } from '@ionic-native/camera';



@Component({
  selector: 'page-status',
  templateUrl: 'status.html'
})
export class StatusPage {
  items = [
    {
      title: 'Courgette daikon',
      content: 'Parsley amaranth tigernut stroot.',
      icon: 'calendar',
      time: { subtitle: '4/16/2013', title: '21:30' }
    },
    {
      title: 'Courgette daikon',
      content: 'Parsley amaranth tigernut silver beet maize fennel spinach. Ricebean black-eyed pea maize scallion green bean spinach cabbage jícama bell pepper carrot onion corn plantain garbanzo. Sierra leone bologi komatsuna celery peanut swiss chard silver beet squash dandelion maize chicory burdock tatsoi dulse radish wakame beetroot.',
      icon: 'calendar',
      time: { subtitle: 'January', title: '29' }
    },
    {
      title: 'Courgette daikon',
      content: 'Parsley amaranth tigernut silver beet maize fennel spinach. Ricebean black-eyed pea maize scallion green bean spinach cabbage jícama bell pepper carrot onion corn plantain garbanzo. Sierra leone bologi komatsuna celery peanut swiss chard silver beet squash dandelion maize chicory burdock tatsoi dulse radish wakame beetroot.',
      icon: 'calendar',
      time: { title: 'Short Text' }
    }
  ]

  currentuserType: any = 0;
  userid: any = 0;
  category: any;
  createdby_id: any;
  selectedpropname: any = "";
  selectedpropid: any = 0;
  imageurl: any = "";
  prop: any;
  user: any;
  propdata: any = [];
  taskdata: any = [];
  pendingtext: any = false;

  activity: any = "";
  todaydate: any = "";
  assigned_properties: any = "";

  constructor(public camera: Camera, public platform: Platform, public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider) {
    let today: any = new Date();
    let dd: any = today.getDate();
    let mm: any = today.getMonth() + 1; //January is 0!
    let yyyy: any = today.getFullYear();

    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }

    this.todaydate = yyyy + '-' + mm + '-' + dd;

    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
    });


    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });
    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.localStorageService.get("selectedpropid").then((val) => {
        if (val != "" && val != null)
          this.selectedpropid = val;

        this.localStorageService.get("assigned_properties").then((aval) => {
          this.assigned_properties = aval;

          if (this.config.GenericPropData.length == 0) {
            this.getPropdata();
          }
          else {
            this.propdata = this.config.GenericPropData;
            this.prop = this.propdata[0].id;
            this.getTaskData();
          }
        });
      });
    });

    this.imageurl = this.config.IMAGE_URL;
  }

  ionViewDidEnter() {


  }

  getPropdata() {
    this.propdata = [];

    var url = this.config.SERVER_URL + '/manage_property.php';
    var creds = {
      action: "ReadL",
      userid: this.createdby_id,
    };
    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait...").then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = JSON.parse(ServerResponse.result);
          for (var i = 0; i < a.length; i++) {
            var addornot = false;
            var as = this.assigned_properties.split(",");
            for (var j = 0; j < as.length; j++) {
              if (as[j] == a[i].id) {
                addornot = true;
                break;
              }
            }
            if (addornot) {
              var photossplit = a[i].photos.split(",");
              this.propdata.push({
                photo: this.config.IMAGE_URL + photossplit[0],
                name: a[i].name,
                address: a[i].address,
                id: a[i].id,
                area: a[i].area
              })
            }
          }
          this.prop = this.propdata[0].id;
          this.config.GenericPropData = this.propdata;
          this.getTaskData();
          //this.propname = this.propdata[0].name;
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  getTaskData() {
    this.taskdata = [];
    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "ReadByProperyOwner",
      property_id: this.prop,
      createdby_id: this.userid,
    };
    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait...").then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = ServerResponse.result;

          for (var i = 0; i < a.length; i++) {
            var photossplit = a[i].photos.split(",");
            var taskname = a[i].task_description.split(" ");
            if (taskname.length >= 2)
              taskname = taskname[0] + " " + taskname[1];
            else
              taskname = a[i].task_description;

            var userpic = "";
            if (a[i].userprofile_pic != null) {
              userpic = this.imageurl + a[i].userprofile_pic;
            }
            else {
              userpic = "assets/img/profile.png";
            }

            var userpic2 = "";
            if (a[i].userprofile_pic2 != null) {
              userpic2 = this.imageurl + a[i].userprofile_pic2;
            }
            else {
              userpic2 = "assets/img/profile.png";
            }

            this.taskdata.push({
              taskid: a[i].taskid,
              username: a[i].username,
              userprofile_pic: userpic,
              userprofile_pic2: userpic2,
              user_type: a[i].user_type,
              task_name: taskname,
              task_description: a[i].task_description,
              setup_date: a[i].setup_date,
              setup_time: a[i].setup_time,
              assigned_person_id: a[i].assigned_person_id,
              assigned_person_name: a[i].assigned_person_name,
              assigned_group: a[i].assigned_group,
              photos: a[i].photos,
              desc: a[i].desc,
              status: a[i].status,
              propid: a[i].propid,
              propname: a[i].propname,
              owner_approval: a[i].owner_approval,
              approved_by: a[i].approved_by,
              creation_date: a[i].creation_date,
              complete_date: a[i].complete_date,
              photo: this.imageurl + photossplit[0]
            })
          }

        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }


  opentaskpage(params) {
    if (!params) params = {};
    this.navCtrl.push(TaskDoneOwnerPage, params);
  }
}
