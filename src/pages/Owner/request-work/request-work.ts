import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../../../providers/config';
import { Camera, CameraOptions } from '@ionic-native/camera';

@Component({
  selector: 'page-request-work',
  templateUrl: 'request-work.html'
})
export class RequestWorkPage {
  taskdata: any = [];
  propdata: any = [];
  prop: any = -1;

  userid: any = 0;
  createdby_id: any = 0;
  currentuserType: any;
  selectedpropid: any = 0;

  description: any = "";

  public filterQuery = "";
  public rowsOnPage = 2;
  public sortBy = "email";
  public sortOrder = "asc";



  Photosarray: any = [];

  photosdata: any = [];
  descdata: any = [];

  deletepicarray: any = [];

  eid: any = 0;
  imageurl: any = "";
  setupdate: any = "";
  setuptime: any = "";
  usergroup: any = 0;

  areaname: any = "";
  assigned_properties: any = "";
  constructor(public platform: Platform, public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider, public camera: Camera) {

  }

  ionViewDidEnter() {
    //this.localStorageService.get("selectedarea").then((val) => {
    //this.areaname = val;
    //});
    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });
    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.localStorageService.get("selectedpropid").then((val) => {
        if (val != "" && val != null)
          this.selectedpropid = val;

        this.localStorageService.get("assigned_properties").then((aval) => {
          this.assigned_properties = aval;

          if (this.config.GenericPropData.length == 0) {
            this.getPropdata();
          }
          else {
            this.propdata = this.config.GenericPropData;
            this.prop = this.propdata[0].id;
            this.getTaskData();
          }
        });
      });
    });
    this.imageurl = this.config.IMAGE_URL;
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 50,
      targetWidth: 300,
      targetHeight: 300,
      destinationType: this.camera.DestinationType.DATA_URL,//this.platform.is('ios') ? this.camera.DestinationType.FILE_URI : this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {

      let base64Image = null;
      base64Image = "data:image/jpeg;base64," + imageData;
      //add photo to the array of photos
      var name = this.config.stringGen(10);
      this.Photosarray.push({ "name": name, "src": base64Image, "desc": "" });

    }, (error) => {
      console.debug("Unable to obtain picture: " + error, "app");
      console.log(error);
    });
  }



  imageRemoved(a) {
    this.Photosarray.splice(this.Photosarray.indexOf(a.src), 1);
    //alert(this.propPhotosarray.length);
  }

  deletePhoto(name) {
    this.Photosarray.splice(this.Photosarray.indexOf(name), 1);
  }

  deletePhoto1(name) {
    this.photosdata.splice(this.photosdata.indexOf(name), 1);
  }

  deletePic(p) {
    var foundornot = false;
    for (var i = 0; i < this.deletepicarray.length; i++) {
      // alert(this.deletepicarray[i].name +"    " +p.name)
      if (this.deletepicarray[i].name == p.name) {
        foundornot = true;
        this.deletepicarray.splice(i, 1);
        break;
      }
    }

    if (foundornot == false)
      this.deletepicarray.push({ "name": p.name, "status": true });

  }

  deletePicArray() {
    for (var i = 0; i < this.deletepicarray.length; i++) {
      this.photosdata.splice(this.photosdata.indexOf(this.deletepicarray[i].name), 1);
      this.Photosarray.splice(this.Photosarray.indexOf(this.deletepicarray[i].name), 1);
    }
    this.deletepicarray = [];
  }


  sendImage(a, count): Promise<{}> {
    return new Promise((resolve) => {
      var url = this.config.SERVER_URL + '/upload_image.php';
      var creds = a;

      var msg = "Please wait... Sending image to server...  ";
      this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
        try {

          if (ServerResponse != "Error") {
            if (ServerResponse.status == "Success") {
              resolve(count);
            }
            else {
              resolve("error");
            }
          }
          else {
            resolve("error");
             this.config.presentToastError(this.config.internetConnectionErrorMsg);
          }
        }
        catch (e) {
          resolve("error");
           this.config.presentToastError(this.config.internetConnectionErrorMsg);
        }
      });
    });
  }

  validatePage() {
    if (this.prop == -1) {
      this.config.presentToastError("Plaese select property location");
      return false;
    }
    else if (this.usergroup == 0) {
      this.config.presentToastError("Plaese select user group!");
      return false;
    }
    else if (this.Photosarray.length == 0) {
      this.config.presentToastError("Plaese upload atleast one photo!!");
      return false;
    }
    else if (this.description == "") {
      this.config.presentToastError("Please enter task description!");
      return false;
    }
    else {
      return true;
    }
  }

  ReportProblem() {
    if (this.validatePage()) {
      var i = 0;
      //for (var i = 0; i < this.Photosarray.length; i++) {
      this.senddata(i, "Insert");

      //}
    }
  }

  senddata(i, type) {
    this.sendImage(this.Photosarray[i], i).then((Response: any) => {
      if (Response == "error") {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
      else if (Response == (this.Photosarray.length - 1)) {
        this.callserver(type);
      }
      else {
        i++;
        this.senddata(i, type);
      }
    });
  }

  UpdateProblem() {

    if (this.description == "") {
      this.config.presentToastError("Please enter task description!");
    }
    else {
      if (this.Photosarray.length != 0) {
        var i = 0;
        this.senddata(i, "Update");
      }
      else {
        if (this.photosdata.length == 0) {
          this.config.presentToastError("Please upload atleast one photo!!");
        }
        else {
          this.callserver("Update");
        }
      }
    }
  }


  callserver(type) {

    var photosname = "";
    var descriptions = "";
    if (this.eid == 0) {
      for (var j = 0; j < this.Photosarray.length; j++) {
        if (photosname == "") {
          photosname = this.Photosarray[j].name.trim();
          descriptions = this.Photosarray[j].desc.trim();
        }
        else {
          photosname = photosname.trim() + "," + this.Photosarray[j].name.trim();
          descriptions = descriptions.trim() + "," + this.Photosarray[j].desc.trim();
        }
      }
    }
    else {
      for (var j = 0; j < this.Photosarray.length; j++) {
        if (photosname == "") {
          photosname = this.Photosarray[j].name.trim();
          descriptions = this.Photosarray[j].desc.trim();
        }
        else {
          photosname = photosname.trim() + "," + this.Photosarray[j].name.trim();
          descriptions = descriptions.trim() + "," + this.Photosarray[j].desc.trim();
        }
      }
      for (var i = 0; i < this.photosdata.length; i++) {
        if (photosname == "") {
          photosname = this.photosdata[i].name.trim();
          descriptions = this.photosdata[i].desc.trim();
        }
        else {
          photosname = photosname.trim() + "," + this.photosdata[i].name.trim();
          descriptions = descriptions.trim() + "," + this.photosdata[i].desc.trim();
        }
      }
    }


    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: type,
      taskid: this.eid,
      created_by_id: this.userid,
      property_admin_id: this.createdby_id,
      property_id: this.prop,
      task_name: "",
      task_description: this.description,
      setup_date: this.setupdate,
      setup_time: this.setuptime,
      assigned_person_id: "",
      assigned_person_name: "",
      assigned_group: this.usergroup,
      owner_approval: 0,
      photos: photosname.trim(),
      desc: descriptions.trim()
    };

    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.description = "";
          this.eid = 0;
          this.config.presentToastSuucess(ServerResponse.remarks);
          this.Photosarray = [];
          this.photosdata = [];
          this.getTaskData();
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  getPropdata() {
    this.propdata = [];

    var url = this.config.SERVER_URL + '/manage_property.php';
    var creds = {
      action: "ReadL",
      userid: this.createdby_id,
    };
    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait...").then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = JSON.parse(ServerResponse.result);
          for (var i = 0; i < a.length; i++) {
            var addornot = false;
            var as = this.assigned_properties.split(",");
            for (var j = 0; j < as.length; j++) {
              if (as[j] == a[i].id) {
                addornot = true;
                break;
              }
            }
            if (addornot) {
              var photossplit = a[i].photos.split(",");
              this.propdata.push({
                photo: this.config.IMAGE_URL + photossplit[0],
                name: a[i].name,
                address: a[i].address,
                id: a[i].id,
                area: a[i].area
              })
            }
          }
          this.prop = this.propdata[0].id;
          this.config.GenericPropData = this.propdata;
          this.getTaskData();
          //this.propname = this.propdata[0].name;
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }
  
  getTaskData() {
    this.taskdata = [];

    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "ReadByCreatedId",
      created_by_id: this.userid,
    };
    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var resultarray = JSON.parse(ServerResponse.result);
          var a = [];
          for (var i = 0; i < resultarray.length; i++) {
            var photo = resultarray[i].photos;
            var photosplit = photo.split(",");
            if (resultarray[i].property_id == this.selectedpropid) {
              a.push({
                photo: this.imageurl + photosplit[0],
                task_description: resultarray[i].task_description,
                property: resultarray[i].property,
                property_id: resultarray[i].property_id,
                taskid: resultarray[i].id,
                photos: resultarray[i].photos,
                desc: resultarray[i].desc,
              });
            }
          }
          this.taskdata = a;

        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  changePhotoDesc(i, value) {
    //this.descdata[j] = desc;
    this.Photosarray[i].desc = value._value;
  }
  changePhotoDesc1(j, value) {
    //this.descdata[j] = desc;
    this.photosdata[j].desc = value._value;
  }

  editTask(u) {
    var a = [];
    var d = [];
    this.photosdata = [];
    //for (var i = 0; i < resultarray.length; i++) {
    this.eid = u.taskid;
    this.description = u.task_description;
    a = u.photos.split(",");

    if (u.desc != null)
      d = u.desc.split(",");
    for (var i = 0; i < a.length; i++) {
      var photo = a[i];
      var desc = "";
      //alert(d.length);

      if (d.length > 1)
        desc = d[i];


      this.photosdata.push({ "name": photo, "desc": desc });
    }
  }

}
