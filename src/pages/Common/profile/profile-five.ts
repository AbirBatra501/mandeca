import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ConfigProvider } from '../../../providers/config';
import { publitioApi } from 'publitio_js_sdk';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { map } from 'rxjs/operators';
var publitio = publitioApi('D9dPPvn4Pj6ne7s4TzaR', 'Jqld6aHZbSzLP846zTSVk0Af8e5rq9dp');
//@IonicPage()
@Component({
  selector: 'page-profile-five',
  templateUrl: 'profile-five.html',
})
export class ProfileFivePage {

  user = {
    name: '',
    profileImage: 'assets/img/profile.png',
    coverImage: 'assets/img/background/background-5.jpg',
    occupation: '',
    location: '',
    description: '',
    address: '',
    phone: '',
    email: '',
    whatsapp: '',
  };

  userid: any = 0;



  constructor(public navCtrl: NavController, public localStorageService: Storage, public camera: Camera, public config: ConfigProvider, private httpClient: HttpClient) { }

  ionViewDidEnter() {


    this.localStorageService.get("profile_pic").then((pval) => {
      //alert(this.config.IMAGE_URL + pval + ".png");
      if (pval != "" && pval != null)
        this.user.profileImage = this.config.publitioPath + this.config.publicIoThumbnail + this.config.publitioUploadProfileDirPath + "/" + pval;
    });

    this.localStorageService.get("firstname").then((fval) => {
      this.localStorageService.get("lastname").then((lval) => {
        this.user.name = fval + " " + lval;
      });
    });

    this.localStorageService.get("email").then((evalu) => {
      this.user.email = evalu;
    });

    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
    });

    this.localStorageService.get("mobileno").then((mval) => {
      this.user.phone = mval;
    });

    this.localStorageService.get("address").then((aval) => {
      this.user.address = aval;
    });

  }

  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,//this.platform.is('ios') ? this.camera.DestinationType.FILE_URI : this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {
      let base64Image = null;
      base64Image = "data:image/jpeg;base64," + imageData;
      var name = this.config.stringGen(10) + ".png";
      //Usage example:
      var file = this.dataURLtoFile(base64Image, name);
      var newImageObject = { "name": name, file: file, "src": base64Image, desc: "", progress: 0, uploaded: false };

      this.uploadFileToServer(newImageObject);
    }, (error) => {
      console.debug("Unable to obtain picture: " + error, "app");
      console.log(error);
    });
  }

  dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
      bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while (n--) {
      u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, { type: mime });
  }


  uploadFileToServer(imageObject) {
    this.config.presentLoader("uploading image to server");
    this.sendfileToServer(imageObject).subscribe(
      (res: any) => {
       
        if (res.progress)
          imageObject.progress = res.progress;
        else if (res.event) {
          if (res.event.status == 201) {
            this.config.hideLoader();
            imageObject.uploaded = true;
            this.callserver(imageObject);
          }
          else {
            this.config.hideLoader();
            this.config.presentToastError(this.config.internetConnectionErrorMsg);
          }
        }
      },
      (err) => {
        this.config.hideLoader();
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    );
  }


  sendfileToServer(imageObject) {
    const formData = new FormData();
    formData.append('file', imageObject.file);
    var uploadURL = publitio.uploadUrlSigned("file", { folder: this.config.publitioUploadProfileDirPath });
    return this.httpClient.post<any>(uploadURL, formData, {
      reportProgress: true,
      observe: 'events'
    }).pipe(map((event) => {
      switch (event.type) {
        case HttpEventType.UploadProgress:
          const progress = Math.round(100 * event.loaded / event.total);
          return { progress: progress };
        case HttpEventType.Response:
          return { event: event };
        default:
          return `Unhandled event: ${event.type}`;
      }
    }));

  }


  callserver(imageObject) {
    var url = this.config.SERVER_URL + '/manage_user.php';
    var creds = {
      action: "UpdatePhoto",
      userid: this.userid,
      profile_pic: imageObject.name
    };

    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        this.config.presentToastSuucess(ServerResponse.remarks);
        if (ServerResponse.status == "Success") {
          this.user.profileImage = this.config.publitioPath + this.config.publicIoThumbnail2 + this.config.publitioUploadProfileDirPath + "/" + imageObject.name;
          this.localStorageService.set("profile_pic", imageObject.name);
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }


}
