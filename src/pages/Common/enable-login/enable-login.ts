import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TabsControllerCleanerPage } from '../../Cleaner/tabs-controller/tabs-controller';
import { SocialSharing } from '@ionic-native/social-sharing';

@Component({
  selector: 'page-enable-login',
  templateUrl: 'enable-login.html'
})
export class EnableLoginPage {

  constructor(public navCtrl: NavController, public socialSharing: SocialSharing) {
  }

  shareInformation() {
    //this.navCtrl.setRoot(TabsControllerPage);
    // Check if sharing via email is supported
    this.socialSharing.canShareViaEmail().then(() => {
      // Sharing via email is possible
      // Share via email
      this.socialSharing.shareViaEmail('Body', 'Subject', ['recipient@example.org']).then(() => {
        // Success!
      }).catch(() => {
        // Error!
      });
    }).catch(() => {
      // Sharing via email is not possible
    });

    //shareViaWhatsApp(message, image, url)


  }
}
