import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ConfigProvider } from '../../../providers/config';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Storage } from '@ionic/storage';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'page-workflow',
  templateUrl: 'workflow.html'
})
export class WorkflowPage {

  userid: any = 0;
  createdby_id: any = 0;

  currentuserType: any;

  category: any;
  workflowdata: any = [];
  categorydata: any = [];

  video_link: any = "";
  description: any = "";
  pdf_link: any = "";
  selectedpropid: any = 0;
  constructor(public iab: InAppBrowser, public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider) {

  }

  ionViewDidEnter() {
    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });
    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.getdata();
    });

  }

  changedata() {
    for (var i = 0; i < this.workflowdata.length; i++) {
      if (this.category == this.workflowdata[i].id) {
        this.category = this.workflowdata[i].id;
        this.video_link = this.workflowdata[i].video_link;
        this.video_link = this.video_link.replace("watch?v=", "embed/");
        this.pdf_link = this.workflowdata[i].pdf_link;
        this.description = this.workflowdata[i].description;
        break;
      }
    }
  }


  getdata() {
    this.workflowdata = [];

    var url = this.config.SERVER_URL + '/manage_workflow.php';
    var creds = {
      action: "Read",
      createdby_id: this.createdby_id,
    };
    var msg = "Please wait...";

    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var catdata = JSON.parse(ServerResponse.result);

          for (var i = 0; i < catdata.length; i++) {
            var assignedto = catdata[i].assigned_to;
            for (var j = 0; j < assignedto.length; j++) {
              if (assignedto[j] == this.currentuserType) {
                this.workflowdata.push(catdata[i]);
                break;
              }
            }
          }
          this.category = this.workflowdata[0].id;

          this.video_link = this.workflowdata[0].video_link;
          this.video_link = this.video_link.replace("watch?v=", "embed/");
          this.pdf_link = this.workflowdata[0].pdf_link;
          this.description = this.workflowdata[0].description;
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  openpdf() {
    this.iab.create(this.pdf_link, "_self");
  }
}
