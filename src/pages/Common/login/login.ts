import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { EnableLoginPage } from '../enable-login/enable-login';
import { ConfigProvider } from '../../../providers/config';
import { Storage } from '@ionic/storage';

import { TabsControllerCleanerPage } from '../../Cleaner/tabs-controller/tabs-controller';
import { TabsControllerTechPage } from '../../Tech/tabs-controller/tabs-controller';
import { TabsControllerAdminPage } from '../../Admin/tabs-controller/tabs-controller';
import { TabsControllerOwnerPage } from '../../Owner/tabs-controller/tabs-controller';
import { LocatePage } from '../locate/locate';

import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  msg:any = "Please wait..";
  email: any;
  constructor(public fb: Facebook, public navCtrl: NavController, public navParams: NavParams, public config: ConfigProvider, public localStorageService: Storage) {
    // If we navigated to this page, we will have an item available as a nav param
    /*  this.localStorageService.get("createdby_id").then((val) => {
        if (val != null && val != "") {
          this.navCtrl.push(TabsControllerPage);
        }
      });*/
  }

  loginviaFb() {
    /* this.fb.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
      this.fb.api('me?fields=id,name,email,first_name)', []).then(profile => {
        this.email = profile['email'];
        alert(this.emai);
      });
    });*/
    this.fb.login(['public_profile', 'user_friends', 'email'])
      .then((res: FacebookLoginResponse) => alert('Logged into Facebook!' + JSON.stringify(res)))
      .catch(e => alert('Error logging into Facebook' + JSON.stringify(e)));
  }

  userData: any;



  loginWithFB() {
    this.fb.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
      this.fb.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', []).then(profile => {
        this.userData = { email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name'] }
        //alert(JSON.stringify(this.userData));
        this.email = this.userData.email;
        this.doLogin();
      });
    });
  }

  doLogin() {
    var url = this.config.SERVER_URL + '/process_login.php';
    var creds = JSON.stringify({ email: this.email, pwd: "" });
    this.config.CallWebApi(url, creds, this.msg).then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var j = JSON.parse(ServerResponse.result);
          this.localStorageService.set("userid", j.userid);
          this.localStorageService.set("assigned_properties", j.assigned_properties);
          this.localStorageService.set("createdby_id", j.createdby_id);
          this.localStorageService.set("profile_pic", j.profile_pic);
          this.localStorageService.set("firstname", j.firstname);
          this.localStorageService.set("lastname", j.lastname);
          this.localStorageService.set("email", j.email);
          this.localStorageService.set("mobileno", j.mobileno);
          this.localStorageService.set("dob", j.dob);
          this.localStorageService.set("address", j.address);
          this.localStorageService.set("usertype", j.user_type);


          this.localStorageService.get("selectedarea").then((sval) => {
            if (j.user_type == 3)//for owner
              this.navCtrl.setRoot(TabsControllerOwnerPage);
            else if (sval == "")
              this.navCtrl.setRoot(LocatePage);
            else if (j.user_type == 2)//for admin
              this.navCtrl.setRoot(TabsControllerAdminPage);
            else if (j.user_type == 5)//for cleaner
              this.navCtrl.setRoot(TabsControllerCleanerPage);
            else if (j.user_type == 4)//for tech
              this.navCtrl.setRoot(TabsControllerTechPage);
          });


        }
        else {
          this.config.presentToastSuucess(ServerResponse.remarks);
          if (ServerResponse.remarks != "User Not Found")
            this.navCtrl.push(EnableLoginPage);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });

  }

  

  getproptypedata() {
    var url = this.config.SERVER_URL + '/manage_proptype.php';
    var creds = {
      action: "Read"
    };
   
    this.config.CallWebApi(url, JSON.stringify(creds),this.msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.localStorageService.set("proptypedata", ServerResponse.result);
          this.getAreaData();
        }
        else {
          this.config.presentToastSuucess(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  getAreaData() {
    let adata: any;
    var url = this.config.SERVER_URL + '/manage_area.php';
    var creds = {
      action: "Read"
    };
    this.config.CallWebApi(url, JSON.stringify(creds),this.msg).then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.localStorageService.set("areadata", ServerResponse.result);
          this.getUserTypeData();
        }
        else {
          this.config.presentToastSuucess(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  getUserTypeData() {
    var url = this.config.SERVER_URL + '/manage_usertype.php';
    var creds = {
      action: "Read"
    };
    this.config.CallWebApi(url, JSON.stringify(creds),this.msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.localStorageService.set("usertypedata", ServerResponse.result);
          //this.router.navigateByUrl('/CommonModule/dashboard');
        }
        else {
          this.config.presentToastSuucess(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  
}