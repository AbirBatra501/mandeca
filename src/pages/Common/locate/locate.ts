import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../../../providers/config';
import { TabsControllerCleanerPage } from '../../Cleaner/tabs-controller/tabs-controller';
import { TabsControllerTechPage } from '../../Tech/tabs-controller/tabs-controller';
import { TabsControllerAdminPage } from '../../Admin/tabs-controller/tabs-controller';
import { TabsControllerOwnerPage } from '../../Owner/tabs-controller/tabs-controller';

@Component({
  selector: 'page-locate',
  templateUrl: 'locate.html'
})
export class LocatePage {
  propdata: any = [];
  areadata: any = [];
  area: any = "All";

  currentuserType: any = 0;
  userid: any = 0;
  createdby_id: any;
  selectedpropid: any = 0;
  assigned_properties: any = "";


  constructor(public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider, public events: Events) {

    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.localStorageService.get("selectedpropid").then((val) => {
        if (val != "" && val != null)
          this.selectedpropid = val;

        this.localStorageService.get("usertype").then((val) => {
          this.currentuserType = val;
          this.localStorageService.get("userid").then((val) => {
            this.userid = val;
            this.localStorageService.get("assigned_properties").then((aval) => {
              this.assigned_properties = aval;
              //alert(this.config.GenericPropData.length);
              if (this.config.GenericPropData.length == 0) {
                this.getPropdata();
              }
              else {
                this.propdata = this.config.GenericPropData;
              }
            });
          });
        });
      });
    });
    this.events.publish('setusernameandmenu', 'All');

  }

  getPropdata() {
    this.propdata = [];

    var url = this.config.SERVER_URL + '/manage_property.php';
    var creds = {};
    if (this.currentuserType == 2) {//for admin
      creds = {
        action: "ReadL",
        userid: this.userid,
      };
    }
    else {
      creds = {
        action: "ReadL",
        userid: this.createdby_id,
      };
    }
    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = JSON.parse(ServerResponse.result);
          for (var i = 0; i < a.length; i++) {
            var addornot = false;
            var photossplit = a[i].photos.split(",");
            if (this.assigned_properties == "" || this.assigned_properties == null) {
              this.propdata.push({
                photo: this.config.publitioPath + this.config.publicIoThumbnail + this.config.publitioUploadDirPath + "/" + photossplit[0],
                name: a[i].name,
                address: a[i].address,
                propid: a[i].id,
                area: a[i].area
              })
            }
            else {
              var as = this.assigned_properties.split(",");
              for (var j = 0; j < as.length; j++) {
                if (as[j] == a[i].id) {
                  addornot = true;
                  break;
                }
              }
              if (addornot) {
                this.propdata.push({
                  photo: this.config.publitioPath + this.config.publicIoThumbnail + this.config.publitioUploadDirPath + "/" + photossplit[0],
                  name: a[i].name,
                  address: a[i].address,
                  propid: a[i].id,
                  area: a[i].area
                })
              }
            }
            if (this.areadata.length == 0) {
              this.areadata.push(a[i].area);
            }
            else {
              var entryornot: any = false;
              for (var j = 0; j < this.areadata.length; j++) {
                if (a[i].area == this.areadata[j]) {
                  entryornot = false;
                  break;
                }
                else {
                  entryornot = true;
                }
              }
              if (entryornot == true)
                this.areadata.push(a[i].area);
            }
          }
          this.area = "All";
          this.config.GenericPropData = this.propdata;

          //this.propdata = JSON.parse(ServerResponse.result);
          //this.propname = this.propdata[0].name;
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  selectProperty(propid, proparea) {
    this.localStorageService.set("selectedpropid", propid);
    this.localStorageService.set("selectedarea", proparea);
    this.selectedpropid = propid;

    this.localStorageService.get("usertype").then((val) => {
      if (val != "" && val != null) {
        if (val == 3)//for owner
          this.navCtrl.setRoot(TabsControllerOwnerPage);
        else if (val == 2)//for admin
          this.navCtrl.setRoot(TabsControllerAdminPage);
        else if (val == 4)//for tech
          this.navCtrl.setRoot(TabsControllerTechPage);
        else if (val == 5)//for cleaner
          this.navCtrl.setRoot(TabsControllerCleanerPage);
      }
    });

  }


}
