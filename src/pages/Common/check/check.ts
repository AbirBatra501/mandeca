import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../../../providers/config';
import { TaskNamePage } from '../../Cleaner/task-name/task-name';
import { LocatePage } from '../locate/locate';

@Component({
    selector: 'page-check',
    templateUrl: 'check.html'
})
export class CheckPage {
    shownGroup = null;

    currentuserType: any = 0;
    userid: any = 0;
    category: any;
    prop: any;
    createdby_id: any;
    selectedpropid: any = 0;

    taskdata: any = [];

    checklistdata: any = [];

    task_name: any = "";
    task_description: any = "";
    task_id: any = 0;
    property_name: any = "";

    photosdata: any = [];

    StratTime: any = "";
    EndTime: any = "";
    imageurl: any = "";

    selectedpropname: any = "";

    username: any = "";


    constructor(public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider) {

    }

    ionViewDidEnter() {
        this.localStorageService.get("selectedarea").then((val) => {
            this.selectedpropname = val;
            /* if (val == "" || val == null)
                this.navCtrl.setRoot(LocatePage); */
        });
        this.localStorageService.get("userid").then((val) => {
            this.userid = val;
        });

        this.localStorageService.get("usertype").then((val) => {
            this.currentuserType = val;
        });
        this.localStorageService.get("createdby_id").then((val) => {
            this.createdby_id = val;
            this.localStorageService.get("selectedpropid").then((val) => {
                if (val != "" && val != null)
                    this.selectedpropid = val;
                this.getCheckedlistdata();
            });
        });

        this.localStorageService.get("firstname").then((fval) => {
            if (fval != "" && fval != null) {
                this.localStorageService.get("lastname").then((lval) => {
                    this.username = fval + " " + lval;
                });
            }
        });


        this.imageurl = this.config.IMAGE_URL;
    }

    toggleGroup(group) {
        if (this.isGroupShown(group)) {
            this.shownGroup = null;
        } else {
            this.shownGroup = group;
        }
    }

    isGroupShown(group) {
        return this.shownGroup === group;
    }






    public myfunc(event: Event) {
        // carouselLoad will trigger this funnction when your load value reaches
        // it is helps to load the data by parts to increase the performance of the app
        // must use feature to all carousel
    }


    getCheckedlistdata() {
        this.checklistdata = [];

        var url = this.config.SERVER_URL + '/manage_check.php';
        var creds = {};
        if (this.currentuserType == 2) {//admin
            creds = {
                action: "Checklist",
                property_id: this.selectedpropid,
                user_id: this.userid,
                currentuserType: this.currentuserType
            };
        }
        else {
            creds = {
                action: "Checklist",
                property_id: this.selectedpropid,
                user_id: this.createdby_id,
                currentuserType: this.currentuserType
            };
        }


        var msg = "Please wait...";
        this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

            if (ServerResponse != "Error") {
                if (ServerResponse.status == "Success") {
                    var a = ServerResponse.result;
                    this.setChecklistdata(a);
                    if (this.currentuserType == 5)//in the case of cleaner we are showing task assigned in check screen
                        this.getTaskData();
                }
                else {
                    this.config.presentToastError(ServerResponse.remarks);
                }
            }
            else {
                this.config.presentToastError(this.config.internetConnectionErrorMsg);
            }
        });
    }



    //set checklist data
    setChecklistdata(a) {
        this.checklistdata = a;
        for (var i = 0; i < a.length; i++) {
            var color = "text-dark";
            var data = a[i].data;
            var truecount = 0;

            for (var j = 0; j < data.length; j++) {
                if (data[j].item_checkedOrNot) {
                    truecount++;
                }
            }
            if (truecount == data.length) {
                color = "text-success";
            }
            else if (truecount > 0 && truecount <= data.length)
                color = "text-primary";

            this.checklistdata[i].color = color;

        }
    }




    editcheckedData(d) {
        d.item_checkedOrNot = false;
    }

    checkData(checklistOrInventory, d, qty, i) {

        if (checklistOrInventory == 2 && (qty == "" || qty == null)) {
            this.config.presentToastError("Please enter quantity");
            return;
        }

        if (d.item_checkedOrNot)
            d.item_checkedOrNot = false;
        else
            d.item_checkedOrNot = true;
        d.item_value = qty;
        d.done_by = this.userid;
        d.done_byname = this.username;

        var url = this.config.SERVER_URL + '/manage_check.php';


        var creds = {
            action: "Checked",
            user_id: this.userid,
            property_id: this.selectedpropid,
            currentuserType: this.currentuserType,
            data: JSON.stringify(this.checklistdata)
        };
        // alert(JSON.stringify(creds));
        var msg = "Please wait...";
        this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {


            if (ServerResponse != "Error") {
                if (ServerResponse.status == "Success") {
                    this.category = -1;
                    var a = JSON.parse(ServerResponse.result);
                    var color = "text-dark";
                    var data = a[i].data;
                    var truecount = 0;

                    for (var j = 0; j < data.length; j++) {
                        if (data[j].item_checkedOrNot) {
                            truecount++;
                        }
                    }

                    if (truecount == data.length) {
                        color = "text-success";
                    }
                    else if (truecount > 0 && truecount <= data.length)
                        color = "text-primary";

                    this.checklistdata[i].color = color;
                }
                else {
                    this.config.presentToastError(ServerResponse.remarks);
                    d.item_checkedOrNot = false;
                }
            }
            else {
                this.config.presentToastError(this.config.internetConnectionErrorMsg);
                d.item_checkedOrNot = false;
            }
        });
    }



    getTaskData() {
        this.taskdata = [];

        var url = this.config.SERVER_URL + '/manage_task.php';
        var creds = {
            action: "ReadByAssignedTo",
            userid: this.userid,
            property_id: this.selectedpropid
        };

        var msg = "Please wait...";
        this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

            if (ServerResponse != "Error") {
                if (ServerResponse.status == "Success") {
                    var a = JSON.parse(ServerResponse.result);
                    for (var i = 0; i < a.length; i++) {
                        var photossplit = a[i].photos.split(",");
                        if (a[i].owner_approval == 1) {
                            if (a[i].approved_by == 0)
                                continue;
                        }

                        this.taskdata.push({
                            photo: this.config.publitioPath + this.config.publicIoThumbnail + this.config.publitioUploadDirPath + "/" + photossplit[0],
                            task_description: a[i].task_description,
                            photos: a[i].photos,
                            taskid: a[i].id,
                            property: a[i].property,
                            owner_approval: a[i].owner_approval,
                            approved_by: a[i].approved_by
                        })

                    }
                }
                else {
                    this.config.presentToastError(ServerResponse.remarks);
                }
            }
            else {
                this.config.presentToastError(this.config.internetConnectionErrorMsg);
            }
        });
    }



    opentaskModal(modal, t) {
        this.task_name = t.task_description;
        this.task_description = t.task_description;
        this.task_id = t.taskid;
        this.property_name = t.property;
        var photosSplit = t.photos.split(",");
        //for(var i=)
        this.photosdata = t.photos.split(",");//[t.photos];
        this.getTaskPerformedData(t.taskid);
        modal.show();
    }

    getTaskPerformedData(taskid) {
        this.StratTime = "";
        this.EndTime = "";

        var url = this.config.SERVER_URL + '/manage_task.php';
        var creds = {
            action: "ReadByTaskid",
            task_id: taskid
        };
        var msg = "Please wait...";
        this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

            if (ServerResponse != "Error") {
                if (ServerResponse.status == "Success") {
                    var a = JSON.parse(ServerResponse.result);

                    if (a.length > 0) {
                        this.StratTime = a[0].start_time;
                        this.EndTime = a[0].end_time;
                    }
                }
                else {
                    this.config.presentToastError(ServerResponse.remarks);
                }
            }
            else {
                this.config.presentToastError(this.config.internetConnectionErrorMsg);
            }
        });
    }

    UpdatePerformedTask(ViewTaskModal, type) {

        var url = this.config.SERVER_URL + '/manage_task.php';
        var creds = {
            action: "StartStopTask",
            task_id: this.task_id
        };
        var msg = "Please wait...";
        this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

            if (ServerResponse != "Error") {
                if (ServerResponse.status == "Success") {
                    //var a = JSON.parse(ServerResponse.result);
                    //alert(JSON.stringify(a));
                    this.config.presentToastSuucess(ServerResponse.remarks);
                    //this.getTaskPerformedData(this.task_id);
                    ViewTaskModal.hide();
                    this.getTaskData();
                }
                else {
                    this.config.presentToastError(ServerResponse.remarks);
                }
            }
            else {
                this.config.presentToastError(this.config.internetConnectionErrorMsg);
            }
        });
    }

    opentaskpage(t) {
        this.navCtrl.push(TaskNamePage,
            {
                "task_name": t.task_description,
                "task_description": t.task_description,
                "task_id": t.taskid,
                "property_name": t.property,
                "photos": t.photos
            });
    }

    openlocatepage() {
        this.navCtrl.push(LocatePage);
    }

}
