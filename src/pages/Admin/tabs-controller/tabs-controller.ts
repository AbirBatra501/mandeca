import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { StatusProblemPage } from '../status-problem/status-problem';
import { ReportsPage } from '../reports/reports';
import { PlanningAdminPage } from '../planning/planning';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { Events } from 'ionic-angular/util/events';

@Component({
  selector: 'page-tabs-controller',
  templateUrl: 'tabs-controller.html'
})
export class TabsControllerAdminPage {
  tabindex: any = 0;
  tab1Root: any = StatusProblemPage;
  tab2Root: any = ReportsPage;
  tab3Root: any = PlanningAdminPage;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
    
    this.tabindex = navParams.get('index');;
  }

  ionViewDidLoad() {
    this.events.publish('setusernameandmenu', 'All');
  }
 
}
