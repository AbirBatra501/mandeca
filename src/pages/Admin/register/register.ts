import { Component } from '@angular/core';
import { NavController, Platform, PopoverController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../../../providers/config';
import { LocatePage } from '../../Common/locate/locate';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { PopoverAreaPage } from '../popoverpagearea/popoverpagearea';
import { Events } from 'ionic-angular/util/events';
import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {


  propdata: any = [];
  prop: any = -1;

  userid: any = 0;
  createdby_id: any = 0;
  currentuserType: any;
  selectedpropid: any = 0;

  propname: any = "";

  proptype: any = "";

  latitude: any;
  longitude: any;

  Photosarray: any = [];

  photosdata: any = [];
  descdata: any = [];

  deletepicarray: any = [];

  eid: any = 0;
  imageurl: any = "";

  selectedpropname: any = "";
  areaname: any = "";
  areaid: any = 0;

  proptypedata: any = [];
  areadata: any = [];

  constructor(public geolocation: Geolocation, public popoverCtrl: PopoverController, public platform: Platform, public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider, public camera: Camera, public events: Events) {
    events.subscribe('setareadata', (eventData) => {
      this.areaid = eventData.areaid;
      this.areaname = eventData.area;
    });
  }

  ionViewDidEnter() {
    this.localStorageService.get("selectedarea").then((val) => {
      this.selectedpropname = val;
      if (val == "" || val == null)
        this.navCtrl.setRoot(LocatePage);
    });
    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });
    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.localStorageService.get("selectedpropid").then((val) => {
        if (val != "" && val != null)
          this.selectedpropid = val;
        //this.getTaskData();
      });
    });
    this.getproptypedata();
    this.imageurl = this.config.IMAGE_URL;

    this.geolocation.getCurrentPosition().then((resp) => {
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  getproptypedata() {
    this.proptypedata = [];

    var url = this.config.SERVER_URL + '/manage_proptype.php';
    var creds = {
      action: "Read"
    };
    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait..").then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.proptypedata = JSON.parse(ServerResponse.result);
          this.getPropData();
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }



  presentAreaPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopoverAreaPage);
    popover.present({
      ev: myEvent
    });
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 50,
      targetWidth: 300,
      targetHeight: 300,
      destinationType: this.camera.DestinationType.DATA_URL,//this.platform.is('ios') ? this.camera.DestinationType.FILE_URI : this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {

      let base64Image = null;
      base64Image = "data:image/jpeg;base64," + imageData;
      //add photo to the array of photos
      var name = this.config.stringGen(10);
      this.Photosarray.push({ "name": name, "src": base64Image, "desc": "" });

    }, (error) => {
      console.debug("Unable to obtain picture: " + error, "app");
      console.log(error);
    });
  }



  imageRemoved(a) {
    this.Photosarray.splice(this.Photosarray.indexOf(a.src), 1);
    //alert(this.propPhotosarray.length);
  }

  deletePhoto(name) {
    this.Photosarray.splice(this.Photosarray.indexOf(name), 1);
  }

  deletePhoto1(name) {
    this.photosdata.splice(this.photosdata.indexOf(name), 1);
  }

  deletePic(p) {
    var foundornot = false;
    for (var i = 0; i < this.deletepicarray.length; i++) {
      // alert(this.deletepicarray[i].name +"    " +p.name)
      if (this.deletepicarray[i].name == p.name) {
        foundornot = true;
        this.deletepicarray.splice(i, 1);
        break;
      }
    }

    if (foundornot == false)
      this.deletepicarray.push({ "name": p.name, "status": true });

  }

  deletePicArray() {
    for (var i = 0; i < this.deletepicarray.length; i++) {
      this.photosdata.splice(this.photosdata.indexOf(this.deletepicarray[i].name), 1);
      this.Photosarray.splice(this.Photosarray.indexOf(this.deletepicarray[i].name), 1);
    }
    this.deletepicarray = [];
  }


  sendImage(a, count): Promise<{}> {
    return new Promise((resolve) => {
      var url = this.config.SERVER_URL + '/upload_image.php';
      var creds = a;
      //alert(JSON.stringify(creds));
      var msg = "Please wait... Sending image to server...  ";
      this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
        try {
          //alert(JSON.stringify(ServerResponse));
          if (ServerResponse != "Error") {
            if (ServerResponse.status == "Success") {
              resolve(count);
            }
            else {
              resolve("error");
            }
          }
          else {
            resolve("error");
             this.config.presentToastError(this.config.internetConnectionErrorMsg);
          }
        }
        catch (e) {
          resolve("error");
           this.config.presentToastError(this.config.internetConnectionErrorMsg);
        }
      });
    });
  }

  validatePage() {
    if (this.proptype == "") {
      this.config.presentToastError("Plaese select property type!!");
      return false;
    }
    else if (this.areaname = "") {
      this.config.presentToastError("Plaese select property area!");
      return false;
    }
    else if (this.Photosarray.length == 0) {
      this.config.presentToastError("Plaese upload atleast one photo!!");
      return false;
    }
    else if (this.propname == "") {
      this.config.presentToastError("Please enter propname!");
      return false;
    }
    else {
      return true;
    }
  }

  RegisterProperty() {
    if (this.validatePage()) {
      var i = 0;
      //for (var i = 0; i < this.Photosarray.length; i++) {
      this.senddata(i, "Insert");

      //}
    }
  }

  senddata(i, type) {
    this.sendImage(this.Photosarray[i], i).then((Response: any) => {
      if (Response == "error") {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
      else if (Response == (this.Photosarray.length - 1)) {
        this.callserver(type);
      }
      else {
        i++;
        this.senddata(i, type);
      }
    });
  }

  UpdateProperty() {
    if (this.propname == "") {
      this.config.presentToastError("Please enter task propname!");
    }
    else {
      if (this.Photosarray.length != 0) {
        var i = 0;
        this.senddata(i, "Update");
      }
      else {
        if (this.photosdata.length == 0) {
          this.config.presentToastError("Please upload atleast one photo!!");
        }
        else {
          this.callserver("Update");
        }
      }
    }
  }


  callserver(type) {
    var photosname = "";
    var propnames = "";
    if (this.eid == 0) {
      for (var j = 0; j < this.Photosarray.length; j++) {
        if (photosname == "") {
          photosname = this.Photosarray[j].name.trim();
          propnames = this.Photosarray[j].desc.trim();
        }
        else {
          photosname = photosname.trim() + "," + this.Photosarray[j].name.trim();
          propnames = propnames.trim() + "," + this.Photosarray[j].desc.trim();
        }
      }
    }
    else {
      for (var j = 0; j < this.Photosarray.length; j++) {
        if (photosname == "") {
          photosname = this.Photosarray[j].name.trim();
          propnames = this.Photosarray[j].desc.trim();
        }
        else {
          photosname = photosname.trim() + "," + this.Photosarray[j].name.trim();
          propnames = propnames.trim() + "," + this.Photosarray[j].desc.trim();
        }
      }
      for (var i = 0; i < this.photosdata.length; i++) {
        if (photosname == "") {
          photosname = this.photosdata[i].name.trim();
          propnames = this.photosdata[i].desc.trim();
        }
        else {
          photosname = photosname.trim() + "," + this.photosdata[i].name.trim();
          propnames = propnames.trim() + "," + this.photosdata[i].desc.trim();
        }
      }
    }


    var url = this.config.SERVER_URL + '/manage_property.php';
    var creds = {
      action: type,
      property_id: this.eid,
      createdby_id: this.userid,
      property_type: this.proptype,
      area: this.areaname,
      name: this.propname,
      address: "",
      latitude: this.latitude,
      longitude: this.longitude,
      photos: photosname
    };

    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          try {
            this.propname = "";
            this.eid = 0;
            this.config.presentToastSuucess(ServerResponse.remarks);
            this.Photosarray = [];
            this.photosdata = [];
            this.areaname = "";
            this.proptype = "";
            this.getPropData();
          }
          catch (e) {
            alert(e)
          }
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }


  getPropData() {
    this.propdata = [];

    var url = this.config.SERVER_URL + '/manage_property.php';
    var creds = {
      action: "ReadL",
      userid: this.userid,
    };
    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var resultarray = JSON.parse(ServerResponse.result);
          var a = [];
          for (var i = 0; i < resultarray.length; i++) {
            var photo = resultarray[i].photos;
            var photosplit = photo.split(",");

            a.push({
              photo: this.config.IMAGE_URL + photosplit[0],
              name: resultarray[i].name,
              property_type: resultarray[i].property_type,
              property_type_id: resultarray[i].property_type_id,
              area: resultarray[i].area,
              address: resultarray[i].address,
              latitude: resultarray[i].latitude,
              longitude: resultarray[i].longitude,
              propid: resultarray[i].id,
              owner: resultarray[i].owner,
              photos: resultarray[i].photos
            });
          }

          this.propdata = a;
          this.proptype = -1;

        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  changePhotoDesc(i, value) {
    //this.descdata[j] = desc;
    this.Photosarray[i].desc = value._value;
  }
  changePhotoDesc1(j, value) {
    //this.descdata[j] = desc;
    this.photosdata[j].desc = value._value;
  }

  editProp(u) {
    var a = [];
    var d = [];
    this.photosdata = [];
    //for (var i = 0; i < resultarray.length; i++) {
    this.eid = u.propid;
    this.propname = u.name;
    this.proptype = u.property_type_id;
    this.areaname = u.area;
    a = u.photos.split(",");

    if (u.desc != null)
      d = u.desc.split(",");
    for (var i = 0; i < a.length; i++) {
      var photo = a[i];
      var desc = "";
      //alert(d.length);

      if (d.length > 1)
        desc = d[i];


      this.photosdata.push({ "name": photo, "desc": desc });
    }
    //alert(JSON.stringify(this.photosdata));
  }

  openlocatepage() {
    this.navCtrl.push(LocatePage);
  }

}
