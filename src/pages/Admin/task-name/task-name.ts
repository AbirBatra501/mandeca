import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ConfigProvider } from '../../../providers/config';
import { Storage } from '@ionic/storage';
import { LocatePage } from '../../Common/locate/locate';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { HideWhen } from 'ionic-angular/components/show-hide-when/hide-when';

@Component({
  selector: 'page-task-name',
  templateUrl: 'task-name.html'
})
export class

  TaskNameAdminPage {
  taskname: any = "";
  taskdescription: any = "";
  tphotosdata: any = ""
  taskid: any = 0;
  property_name: any = "";
  imageurl: any = "";

  setupdate: any = "";
  setuptime: any = "";

  userid: any = 0;
  createdby_id: any = 0;
  currentuserType: any;
  selectedpropid: any = 0;


  Photosarray: any = [];

  photosdata: any = [];
  descdata: any = [];
  taskphoto: any = "";

  deletepicarray: any = [];
  selectedpropname: any = "";

  assigned_group: any;
  assignedpersonname: any;
  assigned_person_id: any;


  propertyid: any = 0;
  userdata: any = [];
  user: any;

  hiddenfields: any = false;



  constructor(navParams: NavParams, public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider, public camera: Camera) {
    try {
      //alert(JSON.stringify(navParams));
      this.setupdate = navParams.get('setup_date');
      this.setuptime = navParams.get('setup_time');

      this.taskdescription = navParams.get('task_description');
      var taskdescSplit = this.taskdescription.split(" ");
      if (taskdescSplit.length > 2)
        this.taskname = taskdescSplit[0] + " " + taskdescSplit[1];
      else
        this.taskname = this.taskdescription;
      this.taskid = navParams.get('taskid');
      this.property_name = navParams.get('propname');
      var photos = navParams.get('photos');
      var photos_desc = navParams.get('desc');

      if (photos != null && photos != "") {
        var pdata = photos.split(",");
        //alert(pdata.length);
        for (var k = 0; k < pdata.length; k++) {
          var desc = "";
          if (photos_desc != null && photos_desc != "") {
            this.descdata = photos_desc.split(",");
            desc = this.descdata[k];
          }

          //alert(pdata[k]);
          this.photosdata.push({ "name": pdata[k], "src": pdata[k], "desc": desc });
        }
      }

      this.taskphoto = photos.split(",")[0];
      //this.photosdata = photos.split(",");
      this.assigned_group = navParams.get('assigned_group');
      this.assignedpersonname = navParams.get('assigned_person_name');
      this.assigned_person_id = navParams.get('assigned_person_id');
      this.propertyid = navParams.get('propid');
      this.imageurl = this.config.IMAGE_URL;
      var showornot = navParams.get('showornot');
      if (showornot == 'yes')
        this.hiddenfields = false;
      else
        this.hiddenfields = true;
    }
    catch (e) {
      alert(e);
    }
  }

  ionViewDidEnter() {
    this.localStorageService.get("selectedarea").then((val) => {
      this.selectedpropname = val;
      if (val == "" || val == null)
        this.navCtrl.setRoot(LocatePage);
    });
    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
      //this.getTaskPerformedData(this.taskid);
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });
    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.localStorageService.get("selectedpropid").then((val) => {
        if (val != "" && val != null)
          this.selectedpropid = val;
        this.SelectGroup(this.assigned_group);
      });
    });
    this.imageurl = this.config.IMAGE_URL;
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 50,
      targetWidth: 300,
      targetHeight: 300,
      destinationType: this.camera.DestinationType.DATA_URL,//this.platform.is('ios') ? this.camera.DestinationType.FILE_URI : this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {

      let base64Image = null;
      base64Image = "data:image/jpeg;base64," + imageData;
      //add photo to the array of photos
      var name = this.config.stringGen(10);
      this.Photosarray.push({ "name": name, "src": base64Image, "desc": "" });

    }, (error) => {
      console.debug("Unable to obtain picture: " + error, "app");
      console.log(error);
    });
  }

  imageRemoved(a) {
    this.Photosarray.splice(this.Photosarray.indexOf(a.src), 1);
    //alert(this.propPhotosarray.length);
  }

  deletePhoto(name) {
    this.Photosarray.splice(this.Photosarray.indexOf(name), 1);
  }

  deletePhoto1(name) {
    this.photosdata.splice(this.photosdata.indexOf(name), 1);
  }

  deletePic(p) {
    var foundornot = false;
    for (var i = 0; i < this.deletepicarray.length; i++) {
      // alert(this.deletepicarray[i].name +"    " +p.name)
      if (this.deletepicarray[i].name == p.name) {
        foundornot = true;
        this.deletepicarray.splice(i, 1);
        break;
      }
    }

    if (foundornot == false)
      this.deletepicarray.push({ "name": p.name, "status": true });

  }

  deletePicArray() {
    for (var i = 0; i < this.deletepicarray.length; i++) {
      this.photosdata.splice(this.photosdata.indexOf(this.deletepicarray[i].name), 1);
      this.Photosarray.splice(this.Photosarray.indexOf(this.deletepicarray[i].name), 1);
    }
    this.deletepicarray = [];
  }

  changePhotoDesc(i, value) {
    //this.descdata[j] = desc;
    this.Photosarray[i].desc = value._value;
  }

  changePhotoDesc1(j, value) {
    //this.descdata[j] = desc;
    this.photosdata[j].desc = value._value;
  }

  sendImage(a, count): Promise<{}> {
    return new Promise((resolve) => {
      var url = this.config.SERVER_URL + '/upload_image.php';
      var creds = a;
      //alert(JSON.stringify(creds));
      var msg = "Please wait... Sending image to server...  ";
      this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
        try {
          //alert(JSON.stringify(ServerResponse));
          if (ServerResponse != "Error") {
            if (ServerResponse.status == "Success") {
              resolve(count);
            }
            else {
              resolve("error");
            }
          }
          else {
            resolve("error");
            this.config.presentToastError(this.config.internetConnectionErrorMsg);
          }
        }
        catch (e) {
          resolve("error");
          this.config.presentToastError(this.config.internetConnectionErrorMsg);
        }
      });
    });
  }


  senddata(i, type) {
    this.sendImage(this.Photosarray[i], i).then((Response: any) => {
      if (Response == "error") {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
      else if (Response == (this.Photosarray.length - 1)) {
        this.callserver(type);
      }
      else {
        i++;
        this.senddata(i, type);
      }
    });
  }

  validatePage() {
    if (this.user == "") {
      this.config.presentToastError("Please select user!!");
      return false;
    }
    else if (this.setupdate == "") {
      this.config.presentToastError("Please enter Setupdate!!");
      return false;
    }
    else if (this.setuptime == "") {
      this.config.presentToastError("Please enter Setupdate!!");
      return false;
    } else if (this.taskdescription == "") {
      this.config.presentToastError("Please enter task description!!");
      return false;
    }
    else if (this.Photosarray.length == 0 && this.photosdata.length == 0) {
      this.config.presentToastError("Please upload atleast one photo!!");
    }
    else {
      return true;
    }
  }

  UpdateTask() {
    if (this.validatePage()) {
      if (this.Photosarray.length != 0) {
        var i = 0;
        this.senddata(i, "Update");
        /*for (var i = 0; i < this.Photosarray.length; i++) {
          this.sendImage(this.Photosarray[i], i).then((Response: any) => {
            if (Response != "error" && Response == (this.Photosarray.length - 1)) {
              this.callserver("Update");
            }
          });
        }*/
      }
      else {
        if (this.photosdata.length == 0) {
          this.config.presentToastError("Please upload atleast one photo!!");
        }
        else {
          this.callserver("Update");
        }
      }
    }
  }


  callserver(type) {
    var photosname = "";
    var descriptions = "";

    for (var j = 0; j < this.Photosarray.length; j++) {
      if (photosname == "") {
        photosname = this.Photosarray[j].name.trim();
        descriptions = this.Photosarray[j].desc.trim();
      }
      else {
        photosname = photosname.trim() + "," + this.Photosarray[j].name.trim();
        descriptions = descriptions.trim() + "," + this.Photosarray[j].desc.trim();
      }
    }
    for (var i = 0; i < this.photosdata.length; i++) {
      if (photosname == "") {
        photosname = this.photosdata[i].name.trim();
        descriptions = this.photosdata[i].desc.trim();
      }
      else {
        photosname = photosname.trim() + "," + this.photosdata[i].name.trim();
        descriptions = descriptions.trim() + "," + this.photosdata[i].desc.trim();
      }
    }

    var url = this.config.SERVER_URL + '/manage_task.php';

    var uid = "";
    var uname = "";
    if (this.user.indexOf != -1) {
      var usersplit = this.user.split("@@");
      uid = usersplit[0];
      uname = usersplit[1];
    }

    var creds = {
      action: type,
      taskid: this.taskid,
      created_by_id: this.userid,
      property_id: this.propertyid,
      task_name: "",
      task_description: this.taskdescription,
      setup_date: this.setupdate,
      setup_time: this.setuptime,
      assigned_person_id: uid.trim(),
      assigned_person_name: uname.trim(),
      assigned_group: this.assigned_group,
      owner_approval: 0,
      photos: photosname.trim(),
      desc: descriptions.trim()
    };

    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.config.presentToastSuucess(ServerResponse.remarks);
          this.navCtrl.pop();
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  SelectGroup(usertype) {

    this.assigned_group = usertype;
    //t.assigned_group = usertype;


    this.userdata = [];
    var url = this.config.SERVER_URL + '/manage_user.php';
    var creds = {
      action: "SearchUserbyType",
      property_id: this.propertyid,
      user_type: this.assigned_group,
      createdby_id: this.userid
    };

    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait...").then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.userdata = ServerResponse.result;
          this.user = -1;
          if (this.assigned_person_id != "")
            this.user = this.assigned_person_id + "@@" + this.assignedpersonname.trim();
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });

  }


}
