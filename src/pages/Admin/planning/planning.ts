import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../../../providers/config';
import { LocatePage } from '../../Common/locate/locate';
import { Camera, CameraOptions } from '@ionic-native/camera';


@Component({
  selector: 'page-planning',
  templateUrl: 'planning.html'
})
export class PlanningAdminPage {



  data: any = [];
  propdata: any = [];
  userdata: any = [];

  prop: any = -1;
  approval: any = 0;
  user: any = 0;

  userid: any = 0;
  createdby_id: any = 0;
  description: any = "";
  assigned_group: any = 0;


  currentuserType: any;

  Photosarray: any = [];
  photosdata: any = [];
  descdata: any = [];

  deletepicarray: any = [];

  imageurl: any = "";

  selectedpropname: any = ""; Í
  selectedpropid: any = "";

  constructor(public camera: Camera, public platform: Platform, public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider) {

  }

  ionViewDidEnter() {
    this.approval = 0;
    this.localStorageService.get("selectedarea").then((val) => {
      this.selectedpropname = val;
      if (val == "" || val == null)
        this.navCtrl.setRoot(LocatePage);
    });
    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });
    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.localStorageService.get("selectedpropid").then((val) => {
        if (val != "" && val != null)
          this.selectedpropid = val;
        this.getPropdata();
      });
    });

    this.imageurl = this.config.IMAGE_URL;
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 50,
      targetWidth: 300,
      targetHeight: 300,
      destinationType: this.camera.DestinationType.DATA_URL,//this.platform.is('ios') ? this.camera.DestinationType.FILE_URI : this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {

      let base64Image = null;
      base64Image = "data:image/jpeg;base64," + imageData;
      //add photo to the array of photos
      var name = this.config.stringGen(10);
      this.Photosarray.push({ "name": name, "src": base64Image, "desc": "" });

    }, (error) => {
      console.debug("Unable to obtain picture: " + error, "app");
      console.log(error);
    });
  }



  imageRemoved(a) {
    this.Photosarray.splice(this.Photosarray.indexOf(a.src), 1);
    //alert(this.propPhotosarray.length);
  }

  deletePhoto(name) {
    this.Photosarray.splice(this.Photosarray.indexOf(name), 1);
  }

  deletePhoto1(name) {
    this.photosdata.splice(this.photosdata.indexOf(name), 1);
  }

  deletePic(p) {
    var foundornot = false;
    for (var i = 0; i < this.deletepicarray.length; i++) {
      // alert(this.deletepicarray[i].name +"    " +p.name)
      if (this.deletepicarray[i].name == p.name) {
        foundornot = true;
        this.deletepicarray.splice(i, 1);
        break;
      }
    }

    if (foundornot == false)
      this.deletepicarray.push({ "name": p.name, "status": true });

  }

  deletePicArray() {
    for (var i = 0; i < this.deletepicarray.length; i++) {
      this.photosdata.splice(this.photosdata.indexOf(this.deletepicarray[i].name), 1);
      this.Photosarray.splice(this.Photosarray.indexOf(this.deletepicarray[i].name), 1);
    }
    this.deletepicarray = [];
  }

  changePhotoDesc(i, value) {
    //this.descdata[j] = desc;
    this.Photosarray[i].desc = value._value;
  }
  changePhotoDesc1(j, value) {
    //this.descdata[j] = desc;
    this.photosdata[j].desc = value._value;
  }

  sendImage(a, count): Promise<{}> {
    return new Promise((resolve) => {
      var url = this.config.SERVER_URL + '/upload_image.php';
      var creds = a;
      //alert(JSON.stringify(creds));
      var msg = "Please wait... Sending image to server...  ";
      this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
        try {
          //alert(JSON.stringify(ServerResponse));
          if (ServerResponse != "Error") {
            if (ServerResponse.status == "Success") {
              resolve(count);
            }
            else {
              resolve("error");
            }
          }
          else {
            resolve("error");
            this.config.presentToastError(this.config.internetConnectionErrorMsg);
          }
        }
        catch (e) {
          resolve("error");
          this.config.presentToastError(this.config.internetConnectionErrorMsg);
        }
      });
    });
  }

  validatePage() {

    if (this.prop == -1) {
      this.config.presentToastError("Plaese select property!");
      return false;
    }
    else if (this.description == "") {
      this.config.presentToastError("Please enter task description!");
      return false;
    }
    else if (this.Photosarray.length == 0) {
      this.config.presentToastError("Plaese upload atleast one photo!!");
      return false;
    }
    else {
      return true;
    }
  }

  ReportProblem() {

    if (this.validatePage()) {
      var i = 0;
      //for (var i = 0; i < this.Photosarray.length; i++) {
      this.senddata(i);

      //}
    }
    else {

    }
  }

  senddata(i) {
    this.sendImage(this.Photosarray[i], i).then((Response: any) => {
      if (Response == "error") {

        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
      else if (Response == (this.Photosarray.length - 1)) {
        this.callserver();
      }
      else {
        i++;
        this.senddata(i);
      }
    });
  }

  callserver() {
    var photosname = "";
    var description = "";
    for (var j = 0; j < this.Photosarray.length; j++) {
      if (photosname == "") {
        photosname = this.Photosarray[j].name.trim();
        description = "";
      }
      else {
        photosname = photosname.trim() + "," + this.Photosarray[j].name.trim();
        description = description.trim() + "," + "";
      }
    }

    var url = this.config.SERVER_URL + '/manage_task.php';
    var apid = "";
    var apname = "";

    if (this.user != 0) {
      var usersplit = this.user.split("@@");
      apid = usersplit[0];
      apname = usersplit[1];
    }

    var creds = {
      action: "Insert",
      created_by_id: this.userid,
      property_admin_id: this.userid,
      property_id: this.prop,
      task_name: "",
      task_description: this.description,
      setup_date: "",
      setup_time: "",
      assigned_person_id: apid.trim(),
      assigned_person_name: apname.trim(),
      assigned_group: this.assigned_group,
      owner_approval: this.approval,
      photos: photosname.trim(),
      desc: description.trim()
    };

    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait...").then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.description = "";
          this.prop = -1;
          this.assigned_group = 0;
          this.userdata = [];
          this.photosdata = [];
          this.Photosarray = [];
          this.config.presentToastSuucess(ServerResponse.remarks);
          //this.getTaskData();
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }


  SelectGroup(usertype) {
    this.assigned_group = usertype;
    this.getuserData();
  }


  getuserData() {
    this.userdata = [];


    var url = this.config.SERVER_URL + '/manage_user.php';
    var creds = {
      action: "SearchUserbyType",
      property_id: this.prop,
      user_type: this.assigned_group,
      createdby_id: this.userid
    };

    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait...").then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.userdata = ServerResponse.result;
          this.user = 0;
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }



  getPropdata() {
    this.propdata = [];

    var url = this.config.SERVER_URL + '/manage_property.php';
    var creds = {
      action: "ReadL",
      userid: this.userid,
    };
    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait...").then((ServerResponse: any) => {


      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.propdata = JSON.parse(ServerResponse.result);
          this.prop = -1;
          //this.getTaskData();
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  openlocatepage() {
    this.navCtrl.push(LocatePage);
  }

  /* getTaskData() {
     this.data = [];
 
     var url = this.config.SERVER_URL + '/manage_task.php';
     var creds = {
       action: "ReadByCreatedId",
       created_by_id: this.userid,
     };
     this.config.CallWebApi(url, JSON.stringify(creds), "Please wait...").then((ServerResponse: any) => {
 
 
       if (ServerResponse != "Error") {
         if (ServerResponse.status == "Success") {
           var resultarray = JSON.parse(ServerResponse.result);
           var a = [];
           for (var i = 0; i < resultarray.length; i++) {
             var photo = resultarray[i].photos;
             var photosplit = photo.split(",");
 
             a.push({
               photo: this.config.IMAGE_URL + photosplit[0],
               task_description: resultarray[i].task_description,
               property: resultarray[i].property,
             });
           }
 
           this.data = a;
           this.prop = -1;
         }
         else {
           
         }
       }
       else {
          this.config.presentToastError(this.config.internetConnectionErrorMsg);
       }
     });
   }*/





}
