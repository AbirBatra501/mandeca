import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ConfigProvider } from '../../../providers/config';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { Events } from 'ionic-angular/util/events';

@Component({
  selector: 'page-popoverpagearea',
  templateUrl: 'popoverpagearea.html'
})
export class PopoverAreaPage {

  areadata: any = [];
  arearecieveddata: any = [];
  constructor(public navCtrl: NavController, public config: ConfigProvider, public viewCtrl: ViewController, public events: Events) {
    this.getAreaData();
  }

  getAreaData() {
    this.areadata = [];

    var url = this.config.SERVER_URL + '/manage_area.php';
    var creds = {
      action: "Read"
    };
    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait..").then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.areadata = JSON.parse(ServerResponse.result);
          this.arearecieveddata = this.areadata;
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  selectArea(id, area) {
    //alert(id + "  " + area);
    this.events.publish('setareadata', { "areaid": id, "area": area });
    this.viewCtrl.dismiss();
  }
  getAreaItems(ev: any) {

    // Reset items back to all of the items
    this.areadata = this.arearecieveddata;

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.areadata = this.areadata.filter((areadata) => {
        return (areadata.area.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }
}
