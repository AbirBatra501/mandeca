import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { CleaningReportPage } from '../cleaning-report/cleaning-report';
import { TaskNameAdminPage } from '../task-name/task-name';
import { TaskDoneAdminPage } from '../task-done/task-done';
import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../../../providers/config';
import { LocatePage } from '../../Common/locate/locate';





@Component({
  selector: 'page-reports',
  templateUrl: 'reports.html'
})
export class ReportsPage {

  currentuserType: any = 0;
  userid: any = 0;
  category: any;
  createdby_id: any;
  selectedpropname: any = "";
  selectedpropid: any = 0;
  imageurl: any = "";
  prop: any;
  user: any;
  propdata: any = [];
  taskdata: any = [];
  pendingtext: any = false;

  activity: any = "";
  todaydate: any = "";

  constructor(public platform: Platform, public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider) {
    let today: any = new Date();
    let dd: any = today.getDate();
    let mm: any = today.getMonth() + 1; //January is 0!
    let yyyy: any = today.getFullYear();

    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }

    this.todaydate = yyyy + '-' + mm + '-' + dd;
  }

  ionViewDidEnter() {
    this.localStorageService.get("selectedarea").then((val) => {
      this.selectedpropname = val;
      if (val == "" || val == null)
        this.navCtrl.setRoot(LocatePage);
    });
    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });
    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.localStorageService.get("selectedpropid").then((val) => {
        if (val != "" && val != null)
          this.selectedpropid = val;
        this.getPropdata();
      });
    });

    this.imageurl = this.config.IMAGE_URL;
  }

  getPropdata() {
    this.propdata = [];

    var url = this.config.SERVER_URL + '/manage_property.php';
    var creds = {
      action: "ReadL",
      userid: this.userid,
    };
    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait...").then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = JSON.parse(ServerResponse.result);
          for (var i = 0; i < a.length; i++) {
            var photossplit = a[i].photos.split(",");
            this.propdata.push({
              photo: this.config.IMAGE_URL + photossplit[0],
              name: a[i].name,
              address: a[i].address,
              id: a[i].id,
              area: a[i].area
            })
          }
          this.prop = "";//this.propdata[0].id
          this.getTaskData();
          //this.propname = this.propdata[0].name;
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }


  getTaskData() {
    this.taskdata = [];
    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "ReadByPropery",
      property_id: this.prop,
      createdby_id: this.userid,
    };
    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait...").then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = ServerResponse.result;
          for (var i = 0; i < a.length; i++) {
            if (a[i].owner_approval == 0 || (a[i].owner_approval == 1 && a[i].approved_by != 0)) {
              var photossplit = a[i].photos.split(",");
              var taskname = a[i].task_description.split(" ");
              if (taskname.length >= 2)
                taskname = taskname[0] + " " + taskname[1];
              else
                taskname = a[i].task_description;


              this.taskdata.push({
                taskid: a[i].taskid,
                username: a[i].userfirstname + " " + a[i].userlastname,
                userprofile_pic: a[i].userprofile_pic,
                user_type: a[i].user_type,
                task_name: taskname,
                task_description: a[i].task_description,
                setup_date: a[i].setup_date,
                setup_time: a[i].setup_time,
                assigned_person_id: a[i].assigned_person_id,
                assigned_person_name: a[i].assigned_person_name,
                assigned_group: a[i].assigned_group,
                photos: a[i].photos,
                desc: a[i].desc,
                status: a[i].status,
                propid: a[i].propid,
                propname: a[i].propname,
                owner_approval: a[i].owner_approval,
                approved_by: a[i].approved_by,
                complete_date: a[i].complete_date,
                photo: this.imageurl + photossplit[0]
              })

            }

            //console.log(JSON.stringify(this.taskdata));
            if (this.taskdata == "")
              this.pendingtext = false;
            else
              this.pendingtext = true;
          }
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }


  goToCleaningReport(params) {
    if (!params) params = {};
    this.navCtrl.push(CleaningReportPage, {
      propid: params.id,
      propname: params.name
    });
  }

  opentaskpage(t) {
    if (t.status == 1)
      this.navCtrl.push(TaskDoneAdminPage, t);
    else
      this.navCtrl.push(TaskNameAdminPage, t);
  }

  openlocatepage() {
    this.navCtrl.push(LocatePage);
  }

}
