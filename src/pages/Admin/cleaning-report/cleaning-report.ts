import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ConfigProvider } from '../../../providers/config';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { Storage } from '@ionic/storage';
import { LocatePage } from '../../Common/locate/locate';

@Component({
  selector: 'page-cleaning-report',
  templateUrl: 'cleaning-report.html'
})
export class CleaningReportPage {
  shownGroup = null;
  checklistdata: any = [];
  userid: any = 0;
  selectedpropid: any = 0;
  propname: any = "";
  selectedpropname: any = "";
  currentuserType: any;
  createdby_id: any;

  constructor(public navCtrl: NavController, public config: ConfigProvider, public navParams: NavParams, public localStorageService: Storage) {
    this.selectedpropid = navParams.get("propid");
    this.propname = navParams.get("propname");
    this.getCheckedlistdata();
  }

  ionViewDidEnter() {
    this.localStorageService.get("selectedarea").then((val) => {
      this.selectedpropname = val;
      if (val == "" || val == null)
        this.navCtrl.setRoot(LocatePage);
    });
    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });
    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.localStorageService.get("selectedpropid").then((val) => {
        if (val != "" && val != null)
          this.selectedpropid = val;
      });
    });

    //this.imageurl = this.config.IMAGE_URL;
  }

  toggleGroup(group) {
    if (this.isGroupShown(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  isGroupShown(group) {
    return this.shownGroup === group;
  }

  getCheckedlistdata() {
    this.checklistdata = [];

    var url = this.config.SERVER_URL + '/manage_check.php';
    var creds = {
      action: "Checklist",
      property_id: this.selectedpropid,
      user_id: this.userid
    };

    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = ServerResponse.result;
          this.setChecklistdata(a);
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  setChecklistdata(a) {
    alert(JSON.stringify(a));
    this.checklistdata = a;
    for (var i = 0; i < a.length; i++) {
      var color = "text-dark";
      var data = a[i].data;
      var truecount = 0;

      for (var j = 0; j < data.length; j++) {
        if (data[j].item_checkedOrNot) {
          truecount++;
        }
      }

      if (truecount == data.length)
        color = "text-success";
      else if (truecount > 0 && truecount <= data.length)
        color = "text-primary";

      this.checklistdata[i].color = color;

    }
  }

  openlocatepage() {
    this.navCtrl.push(LocatePage);
  }

}
