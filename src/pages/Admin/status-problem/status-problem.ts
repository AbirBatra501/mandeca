import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../../../providers/config';
import { LocatePage } from '../../Common/locate/locate';
import { TaskNameAdminPage } from '../task-name/task-name';



@Component({
  selector: 'page-status-problem',
  templateUrl: 'status-problem.html'
})
export class StatusProblemPage {

  currentuserType: any = 0;
  userid: any = 0;
  category: any;
  prop: any;
  user: any;
  selectedtask: any;
  createdby_id: any;
  activity: any = "";

  pendingtext: any = true;
  CreateUpdateModalHeaderText: any;

  assigned_person_id: any = "";
  assigned_group: any;
  propdata: any = "";
  taskdata: any = [];
  userdata: any = [];
  photosdata: any = [];


  taskphoto: any = "";
  taskname: any;
  propertyid: any;
  propertyname: any;
  taskdescription: any;
  edesc: any = "";
  assignedpersonname: any;
  setupdate: any;
  modalphoto: any = "";

  editenable: any = false;
  Photosarray: any = [];

  eid: any = "";
  edess: any = "";
  imageurl: any = "";

  selectedpropname: any = "";
  selectedpropid: any = 0;

  constructor(public platform: Platform, public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider) {

  }

  ionViewDidEnter() {
    this.localStorageService.get("selectedarea").then((val) => {
      this.selectedpropname = val;
      if (val == "" || val == null)
        this.navCtrl.setRoot(LocatePage);
    });
    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });
    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.localStorageService.get("selectedpropid").then((val) => {
        if (val != "" && val != null)
          this.selectedpropid = val;
        this.getPropdata();
      });
    });

    this.imageurl = this.config.IMAGE_URL;
  }

  getPropdata() {
    this.propdata = [];

    var url = this.config.SERVER_URL + '/manage_property.php';
    var creds = {
      action: "ReadL",
      userid: this.userid,
    };
    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait...").then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.propdata = JSON.parse(ServerResponse.result);
          this.prop = "";//this.propdata[0].id
          this.getTaskData();
          //this.propname = this.propdata[0].name;
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  getTaskData() {
    this.taskdata = [];
    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "ReadByPropery",
      property_id: this.prop,
      createdby_id: this.userid,
    };
    this.config.CallWebApi(url, JSON.stringify(creds), "Please wait...").then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = ServerResponse.result;
          for (var i = 0; i < a.length; i++) {
            if (a[i].owner_approval == 0 || (a[i].owner_approval == 1 && a[i].approved_by != 0)) {
              var photossplit = a[i].photos.split(",");
              var taskname = a[i].task_description.split(" ");
              if (taskname.length >= 2)
                taskname = taskname[0] + " " + taskname[1];
              else
                taskname = a[i].task_description;


              this.taskdata.push({
                taskid: a[i].taskid,
                username: a[i].userfirstname + " " + a[i].userlastname,
                userprofile_pic: a[i].userprofile_pic,
                user_type: a[i].user_type,
                task_name: taskname,
                task_description: a[i].task_description,
                setup_date: a[i].setup_date,
                setup_time: a[i].setup_time,
                assigned_person_id: a[i].assigned_person_id,
                assigned_person_name: a[i].assigned_person_name,
                assigned_group: a[i].assigned_group,
                photos: a[i].photos,
                desc: a[i].desc,
                status: a[i].status,
                propid: a[i].propid,
                propname: a[i].propname,
                owner_approval: a[i].owner_approval,
                approved_by: a[i].approved_by,
                photo: this.imageurl + photossplit[0]
              })

            }

            if (this.taskdata == "")
              this.pendingtext = false;
            else
              this.pendingtext = true;
          }
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  opentaskpage(t,showornot) {
    //alert(JSON.stringify(t));
    t.showornot=showornot;
    //alert("Ashish "+JSON.stringify(t));
    this.navCtrl.push(TaskNameAdminPage, t);
  }

  openlocatepage() {
    this.navCtrl.push(LocatePage);
  }

  changeTaskStatus(status, t) {
    // alert(JSON.stringify(t));
    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "ChangeTaskStatus",
      status: status,
      task_id: t.taskid,
    };

    this.config.CallWebApi(url, JSON.stringify(creds),"Please wait..").then((ServerResponse: any) => {
     
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.config.presentToastSuucess(ServerResponse.remarks);
          this.getTaskData();
        }
        else {
          this.config.presentToastError( ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }


}
