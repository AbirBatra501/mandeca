import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { CheckPage } from '../../Common/check/check';
import { ReportPage } from '../report/report';
import { PlanningPage } from '../planning/planning';
import { Storage } from '@ionic/storage';
import { LocatePage } from '../../Common/locate/locate';
@Component({
  selector: 'page-tabs-controller',
  templateUrl: 'tabs-controller.html'
})
export class TabsControllerCleanerPage {
  tabindex: any = 0;
  tab1Root: any = ReportPage;
  tab2Root: any = CheckPage;
  tab3Root: any = PlanningPage;
  constructor(public localStorageService: Storage,public navCtrl: NavController, public navParams: NavParams, public events: Events) {
    this.localStorageService.get("selectedarea").then((val) => {
      if (val == "" || val == null)
        this.navCtrl.setRoot(LocatePage);
    });
    this.tabindex = navParams.get('index');;
  }

  ionViewDidLoad() {
    this.events.publish('setusernameandmenu', 'All');
  }

}
