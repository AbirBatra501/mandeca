import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { ConfigProvider } from '../../../providers/config';

@Component({
  selector: 'page-task-name',
  templateUrl: 'task-name.html'
})
export class TaskNamePage {
  taskname: any = "";
  taskdescription: any = "";
  photosdata: any = ""
  taskid: any = 0;
  property_name: any = "";
  imageurl: any = "";


  StratTime: any = "";
  EndTime: any = "";
  timerecorded: any = "";
  constructor(public navCtrl: NavController, public navParams: NavParams, public config: ConfigProvider) {
    this.taskname = navParams.get('task_name');
    this.taskdescription = navParams.get('task_description');
    this.taskid = navParams.get('task_id');
    this.property_name = navParams.get('property_name');
    var photos = navParams.get('photos');
    this.photosdata = photos.split(",");//[t.photos];
    this.getTaskPerformedData(this.taskid);
    this.imageurl = this.config.IMAGE_URL;
  }

  getTaskPerformedData(taskid) {
    this.StratTime = "";
    this.EndTime = "";

    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "ReadByTaskid",
      task_id: taskid
    };
    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = JSON.parse(ServerResponse.result);

          if (a.length > 0) {
            this.StratTime = a[0].start_time;
            this.EndTime = a[0].end_time;
            if (this.StratTime != '' && this.EndTime != '') {


              let diffInMs: number = Date.parse(this.EndTime) - Date.parse(this.StratTime);
              //alert(this.EndTime + "  " + this.StratTime)
              //let diffInHours: number = diffInMs / 1000 / 60 / 60;
              //var diffHrs = Math.floor((diffInMs % 86400000) / 3600000); // hours
              var diffMins = Math.round(((diffInMs % 86400000) % 3600000) / 60000);
              this.timerecorded = diffMins + " minutes";
            }

          }
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }




  UpdatePerformedTask(type) {
    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "StartStopTask",
      task_id: this.taskid
    };
    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          if (type == "stop") {
            this.UpdateTask();
          }
          else {
            this.config.presentToastSuucess(ServerResponse.remarks);
            this.getTaskPerformedData(this.taskid);
          }
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }


  UpdateTask() {
    var url = this.config.SERVER_URL + '/manage_task.php';
    var photosname = "";
    var descriptions = "";

    for (var k = 0; k < this.config.uploadPhotosQoeue.length; k++) {
      if (photosname == "") {
        photosname = this.config.uploadPhotosQoeue[k].name.trim();
        descriptions = this.config.uploadPhotosQoeue[k].desc.trim();
      }
      else {
        photosname = photosname.trim() + "," + this.config.uploadPhotosQoeue[k].name.trim();
        descriptions = descriptions.trim() + "," + this.config.uploadPhotosQoeue[k].desc.trim();
      }
    }
    var creds = {
      action: "TaskDone",
      task_id: this.taskid,
      photos: photosname.trim(),
      desc: descriptions.trim()
    };

    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.config.presentToastSuucess(ServerResponse.remarks);
          this.navCtrl.pop();
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

}
