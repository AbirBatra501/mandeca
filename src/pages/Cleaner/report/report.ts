import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../../../providers/config';
import { LocatePage } from '../../Common/locate/locate';
import { Config } from 'ionic-angular/config/config';

@Component({
  selector: 'page-report',
  templateUrl: 'report.html'
})
export class ReportPage {
  taskdata: any = [];
  propdata: any = [];
  prop: any = -1;

  userid: any = 0;
  createdby_id: any = 0;
  currentuserType: any;
  selectedpropid: any = 0;

  description: any = "";

  public filterQuery = "";
  public rowsOnPage = 2;
  public sortBy = "email";
  public sortOrder = "asc";




  eid: any = 0;
  imageurl: any = "";

  areaname: any = "";
  constructor(
    public platform: Platform, public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider) {

    
    this.localStorageService.get("selectedarea").then((val) => {
      this.areaname = val;
      /* if (val == "" || val == null)
        this.navCtrl.setRoot(LocatePage); */
    });
    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });
    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.localStorageService.get("selectedpropid").then((val) => {
        if (val != "" && val != null)
          this.selectedpropid = val;
        this.getTaskData();
      });
    });
    this.imageurl = this.config.IMAGE_URL;


    /*  // list files
     publitio.call('/files/list', 'GET', { offset: '0', limit: '10' })
       .then((response) => { console.log(response) })
       .catch((error) => { console.log(error) }) */
  }

  ionViewDidEnter() {

  }



  validatePage() {
    /*   if (this.Photosarray.length == 0) {
        this.config.presentToastError("Plaese upload atleast one photo!!");
        return false;
      }
      else  */
    if (!this.config.enablePageActions) {
      this.config.presentToastError("Please wait image uploading is in process");
      return false;
    }
    else if (this.description == "") {
      this.config.presentToastError("Please enter task description!");
      return false;
    }
    else {
      return true;
    }
  }

  ReportOrUpdateProblem(type) {
    if (this.validatePage()) {
      this.callserver(type);
    }
  }

  callserver(type) {

    var photosname = "";
    var descriptions = "";

    for (var k = 0; k < this.config.uploadPhotosQoeue.length; k++) {
      if (photosname == "") {
        photosname = this.config.uploadPhotosQoeue[k].name.trim();
        descriptions = this.config.uploadPhotosQoeue[k].desc.trim();
      }
      else {
        photosname = photosname.trim() + "," + this.config.uploadPhotosQoeue[k].name.trim();
        descriptions = descriptions.trim() + "," + this.config.uploadPhotosQoeue[k].desc.trim();
      }
    }

    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: type,
      taskid: this.eid,
      created_by_id: this.userid,
      property_admin_id: this.createdby_id,
      property_id: this.selectedpropid,
      task_name: "",
      task_description: this.description,
      setup_date: "",
      setup_time: "",
      assigned_person_id: "",
      assigned_person_name: "",
      assigned_group: this.currentuserType,
      owner_approval: 0,
      photos: photosname.trim(),
      desc: descriptions.trim()
    };



    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.description = "";
          this.eid = 0;
          this.config.presentToastSuucess(ServerResponse.remarks);
          this.config.uploadPhotosQoeue = [];
          this.getTaskData();
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }


  getTaskData() {
    this.taskdata = [];

    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "ReadByCreatedId",
      created_by_id: this.userid,
    };
    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var resultarray = JSON.parse(ServerResponse.result);

          var a = [];
          for (var i = 0; i < resultarray.length; i++) {
            var photo = resultarray[i].photos;
            var photosplit = photo.split(",");
            if (resultarray[i].property_id == this.selectedpropid) {
              var pic = "assets/img/noimageavailable.png";
              if (photosplit.length > 0 && photosplit[0] != "")
                pic = this.config.publitioPath + this.config.publicIoThumbnail + this.config.publitioUploadDirPath + "/" + photosplit[0]

              a.push({
                photo: pic ,
                task_description: resultarray[i].task_description,
                property: resultarray[i].property,
                property_id: resultarray[i].property_id,
                taskid: resultarray[i].id,
                photos: resultarray[i].photos,
                desc: resultarray[i].desc,
              });
            }
          }

          //alert(JSON.stringify(a));
          this.taskdata = a;

        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }



  editTask(u) {
    //alert(JSON.stringify(u));
    var a = [];
    var d = [];
    this.config.uploadPhotosQoeue = [];
    //for (var i = 0; i < resultarray.length; i++) {
    this.eid = u.taskid;
    this.description = u.task_description;
    a = u.photos.split(",");

    if (u.desc != null)
      d = u.desc.split(",");
    for (var i = 0; i < a.length; i++) {
      var photo = a[i];
      var desc = "";
      if (d.length > 0)
        desc = d[i];

      var displayphoto = this.config.publitioPath + this.config.publicIoThumbnail + this.config.publitioUploadDirPath + "/" + photo;
      var newImageObject = { "name": photo, file: "", "src": displayphoto, desc: desc, progress: -1, uploaded: true };
      this.config.uploadPhotosQoeue.push(newImageObject);
    }
    //alert(JSON.stringify(this.photosdata));
  }

  openlocatepage() {
    this.navCtrl.push(LocatePage);
  }

}
