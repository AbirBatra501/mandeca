import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TaskNamePage1 } from '../task-name/task-name';
import { SubtaskPage } from '../subtask/subtask';
import { ConfigProvider } from '../../../providers/config';
import { LocatePage } from '../../Common/locate/locate';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-to-do',
  templateUrl: 'to-do.html'
})
export class ToDoPage {

  taskdata: any = [];
  selectedpropname: any = "";

  currentuserType: any = 0;
  userid: any = 0;
  createdby_id: any;
  selectedpropid: any = 0;

  imageurl: any = "";
  todaydate: any = "";

  propdata: any = [];
  prop: any = 0;
  assigned_properties: any = ""; ''
  constructor(public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider) {

    let today: any = new Date();
    let dd: any = today.getDate();
    let mm: any = today.getMonth() + 1; //January is 0!
    let yyyy: any = today.getFullYear();

    if (dd < 10) {
      dd = '0' + dd
    }

    if (mm < 10) {
      mm = '0' + mm
    }

    this.todaydate = yyyy + '-' + mm + '-' + dd;

  }

  ionViewDidEnter() {
    this.prop = 0;
    this.localStorageService.get("selectedarea").then((val) => {
      this.selectedpropname = val;
      if (val == "" || val == null)
        this.navCtrl.setRoot(LocatePage);
    });
    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
      this.localStorageService.get("createdby_id").then((val) => {
        this.createdby_id = val;
        this.localStorageService.get("selectedpropid").then((val) => {
          if (val != "" && val != null)
            this.selectedpropid = val;

          this.localStorageService.get("assigned_properties").then((aval) => {
            this.assigned_properties = aval;
            if (this.config.GenericPropData.length == 0) {
              this.getPropdata();
            }
            else {
              this.propdata = this.config.GenericPropData;
              this.getTaskData();
            }
          });
        });
      });
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });



    this.imageurl = this.config.IMAGE_URL;
  }


  getPropdata() {
    this.propdata = [];

    var url = this.config.SERVER_URL + '/manage_property.php';
    var creds = {};
    if (this.currentuserType == 2) {//for admin
      creds = {
        action: "Read",
        userid: this.userid,
      };
    }
    else {
      creds = {
        action: "Read",
        userid: this.createdby_id,
      };
    }
    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = JSON.parse(ServerResponse.result);
          for (var i = 0; i < a.length; i++) {
            var addornot = false;
            var as = this.assigned_properties.split(",");
            for (var j = 0; j < as.length; j++) {
              if (as[j] == a[i].id) {
                addornot = true;
                break;
              }
            }
            if (addornot) {
              var photossplit = a[i].photos.split(",");
              this.propdata.push({
                photo: this.config.IMAGE_URL + photossplit[0],
                name: a[i].name,
                address: a[i].address,
                propid: a[i].id,
                area: a[i].area
              })
            }

          }

          this.config.GenericPropData = this.propdata;
          this.getTaskData();
          //this.propdata = JSON.parse(ServerResponse.result);
          //this.propname = this.propdata[0].name;
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }


  goToTaskName(t) {
    if (!t) t = {};
    this.navCtrl.push(TaskNamePage1, {
      "task_name": t.task_name,
      "task_description": t.task_description,
      "task_id": t.taskid,
      "property_name": t.property,
      "property_type": t.property_type,
      "property_id": t.property_id,
      "property_photos": t.property_photos,
      "setup_date": t.setup_date,
      "photos": t.photos
    });
  }


  getTaskData() {
    this.taskdata = [];
    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "ReadByAssignedTo",
      userid: this.userid,
      property_id: this.prop
    };
    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = JSON.parse(ServerResponse.result);
          for (var i = 0; i < a.length; i++) {
            var photossplit = a[i].photos.split(",");
            var taskname = a[i].task_description.split(" ");
            if (taskname.length >= 2)
              taskname = taskname[0] + " " + taskname[1];
            else
              taskname = a[i].task_description;

            //if (a[i].setup_date != this.todaydate) {
              this.taskdata.push({
                photo: this.config.IMAGE_URL + photossplit[0],
                task_name: taskname,
                task_description: a[i].task_description,
                photos: a[i].photos,
                taskid: a[i].id,
                property: a[i].property,
                property_type: a[i].property_type,
                property_id: a[i].property_id,
                setup_date: a[i].setup_date,
                property_photos: a[i].property_photos
              })
            //}
          }
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  openlocatepage() {
    this.navCtrl.push(LocatePage);
  }
}
