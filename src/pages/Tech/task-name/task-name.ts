import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SubtaskPage } from '../subtask/subtask';
import { BuyMaterialPage } from '../buy-material/buy-material';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { ConfigProvider } from '../../../providers/config';
import { Storage } from '@ionic/storage';
import { LocatePage } from '../../Common/locate/locate';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';
import { MenuController } from 'ionic-angular/components/app/menu-controller';


@Component({
  selector: 'page-task-name',
  templateUrl: 'task-name.html'
})
export class TaskNamePage1 {
  taskname: any = "";
  taskdescription: any = "";
  tphotosdata: any = ""
  taskid: any = 0;
  property_name: any = "";
  imageurl: any = "";

  propphoto: any = "";
  urgentornot: any = "";

  StratTime: any = "";
  EndTime: any = "";
  public ViewModal: any;
  public EditModal: any;


  taskdata: any = [];
  propdata: any = [];
  prop: any = -1;

  userid: any = 0;
  createdby_id: any = 0;
  currentuserType: any;
  selectedpropid: any = 0;


  Photosarray: any = [];

  photosdata: any = [];
  descdata: any = [];

  deletepicarray: any = [];

  materialdata: any = [];

  loading: any;
  selectedpropname: any = "";
  constructor(public menu: MenuController, public loadingCtrl: LoadingController, navParams: NavParams, public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider, public camera: Camera) {

    this.taskname = navParams.get('task_name');
    this.taskdescription = navParams.get('task_description');
    this.taskid = navParams.get('task_id');
    this.property_name = navParams.get('property_name');
    var photos = navParams.get('photos');
    var property_photos = navParams.get('property_photos');
    var property_photos_split = property_photos.split(",");
    this.propphoto = property_photos_split[0];
    this.tphotosdata = photos.split(",");
    this.imageurl = this.config.IMAGE_URL;
  }

  ionViewDidEnter() {
    this.menu.enable(false, 'menu2');
    this.menu.enable(true, 'menu1');
    this.localStorageService.get("selectedarea").then((val) => {
      this.selectedpropname = val;
      if (val == "" || val == null)
        this.navCtrl.setRoot(LocatePage);
    });
    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
      this.getTaskPerformedData(this.taskid);
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });
    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.localStorageService.get("selectedpropid").then((val) => {
        if (val != "" && val != null)
          this.selectedpropid = val;
      });
    });
    this.imageurl = this.config.IMAGE_URL;
  }

  getTaskData() {
    this.taskdata = [];
    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "ReadByCreatedId",
      created_by_id: this.userid
    };
    var msg = "";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      //alert(JSON.stringify(ServerResponse));
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var resultarray = JSON.parse(ServerResponse.result);
          var a = [];
          for (var i = 0; i < resultarray.length; i++) {
            var photo = resultarray[i].photos;
            var photosplit = photo.split(",");
            if (resultarray[i].property_id == this.selectedpropid) {
              a.push({
                photo: this.config.IMAGE_URL + photosplit[0],
                task_description: resultarray[i].task_description,
                property: resultarray[i].property,
                property_id: resultarray[i].property_id,
                taskid: resultarray[i].id,
                photos: resultarray[i].photos,
                desc: resultarray[i].desc,
              });
            }
          }

          this.taskdata = a;

          this.getBuyMaterialData();

        }
        else {
          this.loading.dismiss()
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.loading.dismiss();
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  getBuyMaterialData() {
    this.materialdata = [];

    var url = this.config.SERVER_URL + '/manage_buymaterial.php';
    var creds = {
      action: "Read",
      userid: this.userid
    };

    var msg = "";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var resultarray = JSON.parse(ServerResponse.result);
          var a = [];
          for (var i = 0; i < resultarray.length; i++) {
            var photo = resultarray[i].photos;
            var photosplit = photo.split(",");
            if (resultarray[i].property_id == this.selectedpropid) {
              a.push({
                photo: this.config.IMAGE_URL + photosplit[0],
                buyid: resultarray[i].buyid,
                property_id: resultarray[i].property_id,
                property_name: resultarray[i].property_name,
                task_id: resultarray[i].task_id,
                task_description: resultarray[i].task_description,
                material_name: resultarray[i].material_name,
                shope_id: resultarray[i].shope_id,
                photos: resultarray[i].photos,
                photos_desc: resultarray[i].photos_desc,
                status: resultarray[i].status
              });
            }
          }

          this.materialdata = a;
          this.loading.dismiss();
        }
        else {
          this.loading.dismiss();
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.loading.dismiss();
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }




  getTaskPerformedData(taskid) {
    this.loading = this.loadingCtrl.create({
      content: "Please Wait..."
    });
    this.loading.present();
    this.StratTime = "";
    this.EndTime = "";

    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "ReadByTaskid",
      task_id: taskid
    };
    var msg = "";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

      this.photosdata=[];
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = JSON.parse(ServerResponse.result);

          if (a.length > 0) {
            this.StratTime = a[0].start_time;
            this.EndTime = a[0].end_time;

            if (a[0].photos != null && a[0].photos != "") {
              var pdata = a[0].photos.split(",");
              for (var k = 0; k < pdata.length; k++) {
                var desc = "";
                if (a[0].photos_desc != null && a[0].photos_desc != "") {
                  this.descdata = a[0].photos_desc.split(",");
                  desc = this.descdata[k];
                }

                this.photosdata.push({ "name": pdata[k], "src": pdata[k], "desc": desc });
              }
            }
            //alert(JSON.stringify(this.photosdata));
          }
          this.getTaskData();
        }
        else {
          this.loading.dismiss();
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.loading.dismiss();
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  UpdatePerformedTask(type) {
    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "StartStopTask",
      task_id: this.taskid
    };
    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.config.presentToastSuucess(ServerResponse.remarks);
          this.getTaskPerformedData(this.taskid);
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  openCamera() {
    const options: CameraOptions = {
      quality: 50,
      targetWidth: 300,
      targetHeight: 300,
      destinationType: this.camera.DestinationType.DATA_URL,//this.platform.is('ios') ? this.camera.DestinationType.FILE_URI : this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {

      let base64Image = null;
      base64Image = "data:image/jpeg;base64," + imageData;
      //add photo to the array of photos
      var name = this.config.stringGen(10);
      this.Photosarray.push({ "name": name, "src": base64Image, "desc": "" });

    }, (error) => {
      console.debug("Unable to obtain picture: " + error, "app");
      console.log(error);
    });
  }


  imageRemoved(a) {
    this.Photosarray.splice(this.Photosarray.indexOf(a.src), 1);
    //alert(this.propPhotosarray.length);
  }

  deletePhoto(name) {
    this.Photosarray.splice(this.Photosarray.indexOf(name), 1);
  }

  deletePhoto1(name) {
    this.photosdata.splice(this.photosdata.indexOf(name), 1);
  }

  deletePic(p) {
    var foundornot = false;
    for (var i = 0; i < this.deletepicarray.length; i++) {
      // alert(this.deletepicarray[i].name +"    " +p.name)
      if (this.deletepicarray[i].name == p.name) {
        foundornot = true;
        this.deletepicarray.splice(i, 1);
        break;
      }
    }

    if (foundornot == false)
      this.deletepicarray.push({ "name": p.name, "status": true });

  }

  deletePicArray() {
    for (var i = 0; i < this.deletepicarray.length; i++) {
      this.photosdata.splice(this.photosdata.indexOf(this.deletepicarray[i].name), 1);
      this.Photosarray.splice(this.Photosarray.indexOf(this.deletepicarray[i].name), 1);
    }
    this.deletepicarray = [];
  }

  changePhotoDesc(i, value) {
    //this.descdata[j] = desc;
    this.Photosarray[i].desc = value._value;
  }

  changePhotoDesc1(j, value) {
    //this.descdata[j] = desc;
    this.photosdata[j].desc = value._value;
  }

  sendImage(a, count): Promise<{}> {
    return new Promise((resolve) => {
      var url = this.config.SERVER_URL + '/upload_image.php';
      var creds = a;
      //alert(JSON.stringify(creds));
      var msg = "Please wait... Sending image to server...  ";
      this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
        try {
          //alert(JSON.stringify(ServerResponse));
          if (ServerResponse != "Error") {
            if (ServerResponse.status == "Success") {
              resolve(count);
            }
            else {
              resolve("error");
            }
          }
          else {
            resolve("error");
             this.config.presentToastError(this.config.internetConnectionErrorMsg);
          }
        }
        catch (e) {
          resolve("error");
           this.config.presentToastError(this.config.internetConnectionErrorMsg);
        }
      });
    });
  }


  senddata(i, type) {
    this.sendImage(this.Photosarray[i], i).then((Response: any) => {
      if (Response == "error") {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
      else if (Response == (this.Photosarray.length - 1)) {
        this.callserver(type);
      }
      else {
        i++;
        this.senddata(i, type);
      }
    });
  }

  UpdateTask() {
    if (this.Photosarray.length != 0) {
      var i = 0;
      this.senddata(i, "Update");
    }
    else {
      if (this.photosdata.length == 0) {
        this.config.presentToastError("Please upload atleast one photo!!");
      }
      else {
        this.callserver("Update");
      }
    }
  }


  callserver(type) {
    var photosname = "";
    var descriptions = "";

    for (var j = 0; j < this.Photosarray.length; j++) {
      if (photosname == "") {
        photosname = this.Photosarray[j].name.trim();
        descriptions = this.Photosarray[j].desc.trim();
      }
      else {
        photosname = photosname.trim() + "," + this.Photosarray[j].name.trim();
        descriptions = descriptions.trim() + "," + this.Photosarray[j].desc.trim();
      }
    }
    for (var i = 0; i < this.photosdata.length; i++) {
      if (photosname == "") {
        photosname = this.photosdata[i].name.trim();
        descriptions = this.photosdata[i].desc.trim();
      }
      else {
        photosname = photosname.trim() + "," + this.photosdata[i].name.trim();
        descriptions = descriptions.trim() + "," + this.photosdata[i].desc.trim();
      }
    }

    var url = this.config.SERVER_URL + '/manage_task.php';
    var creds = {
      action: "TaskDone",
      task_id: this.taskid,
      photos: photosname.trim(),
      desc: descriptions.trim()
    };

    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.config.presentToastSuucess(ServerResponse.remarks);
          this.navCtrl.pop();
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }


  goToSubtask(params) {
    if (!params) params = {};
    this.navCtrl.push(SubtaskPage);
  }
  editTask(params) {
    if (!params) params = {};
    this.navCtrl.push(SubtaskPage, params);
  }
  goToMaterialToBuyDescription() {
    var params = { taskid: this.taskid, taskname: this.taskname };
    //alert(JSON.stringify(params));
    this.navCtrl.push(BuyMaterialPage, params);
  }
  editBuyMaterial(params) {
    //alert(JSON.stringify(params));
    if (!params) params = {};
    this.navCtrl.push(BuyMaterialPage, params);
  }
}
