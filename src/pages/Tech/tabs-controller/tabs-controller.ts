import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { ToDoPage } from '../../Tech/to-do/to-do';
import { ToBuyListPage } from '../../Tech/to-buy-list/to-buy-list';
import { UrgentTasksPage } from '../../Tech/urgent-tasks/urgent-tasks'

@Component({
  selector: 'page-tabs-controller',
  templateUrl: 'tabs-controller.html'
})
export class TabsControllerTechPage {

  tabindex: any = 0;
  tab1Root: any = UrgentTasksPage;
  tab2Root: any = ToDoPage;
  tab3Root: any = ToBuyListPage;
  constructor(public navCtrl: NavController, public navParams: NavParams, public events: Events) {
    this.tabindex = navParams.get('index');;
  }

  ionViewDidLoad() {
    this.events.publish('setusernameandmenu', 'All');

  }

}

