import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { ConfigProvider } from '../../../providers/config';
import { Storage } from '@ionic/storage';
import { Events } from 'ionic-angular/util/events';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { NavParams } from 'ionic-angular/navigation/nav-params';

@Component({
  selector: 'page-buy-material',
  templateUrl: 'buy-material.html'
})
export class BuyMaterialPage {

  propdata: any = [];
  taskdata: any = [];
  shopedata: any = [];
  areadata: any = [];
  area: any = "All";

  currentuserType: any = 0;
  userid: any = 0;
  createdby_id: any;
  selectedpropid: any = 0;

  description: any = "";
  Photosarray: any = [];

  photosdata: any = [];
  descdata: any = [];

  deletepicarray: any = [];
  selectedpropname: any = "";

  imageurl: any = "";

  eid: any = 0;

  constructor(public navParams: NavParams, public navCtrl: NavController, public localStorageService: Storage, public menu: MenuController, public config: ConfigProvider, public events: Events, public camera: Camera) {

  }

  ionViewDidEnter() {

    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });
    this.localStorageService.get("createdby_id").then((val) => {
      this.createdby_id = val;
      this.localStorageService.get("selectedpropid").then((val) => {
        if (val != "" && val != null) {
          this.selectedpropid = val;
          this.localStorageService.get("selectedarea").then((val) => {
            this.selectedpropname = val;
            if (this.navParams.get("buyid") == null) {
              this.eid = 0;
              this.config.BuymaterialPropid = this.selectedpropid;
              this.config.BuymaterialPropname = this.selectedpropname;
              this.config.BuymaterialTaskid = this.navParams.get("taskid");
              this.config.BuymaterialTaskname = this.navParams.get("taskname");
            }
            else {
              var a = [];
              var d = [];
              this.photosdata = [];

              //alert(JSON.stringify(this.navParams));
              this.eid = this.navParams.get("buyid");
              this.description = this.navParams.get("material_name");
              this.config.BuymaterialPropid = this.navParams.get("property_id");
              this.config.BuymaterialPropname = this.navParams.get("property_name");

              this.config.BuymaterialTaskid = this.navParams.get("task_id");
              this.config.BuymaterialTaskname = this.navParams.get("task_description");
              var taskname = this.config.BuymaterialTaskname.split(" ");
              if (taskname.length >= 2)
                taskname = taskname[0] + " " + taskname[1];
              else
                taskname = this.navParams.get("task_description");

              this.config.BuymaterialTaskname = taskname;

              this.config.BuymaterialShopes = JSON.parse(this.navParams.get("shope_id"));
              a = this.navParams.get("photos").split(",");
              //alert(a);
              if (this.navParams.get("photos_desc") != null)
                d = this.navParams.get("photos_desc").split(",");

              for (var i = 0; i < a.length; i++) {
                var photo = a[i];
                var desc = "";
                if (d.length > 0)
                  desc = d[i];
                this.photosdata.push({ "name": photo, "desc": desc });
              }
            }
          });
        }
      });
    });
    this.imageurl = this.config.IMAGE_URL;


  }

  openCamera() {
    const options: CameraOptions = {
      quality: 50,
      targetWidth: 300,
      targetHeight: 300,
      destinationType: this.camera.DestinationType.DATA_URL,//this.platform.is('ios') ? this.camera.DestinationType.FILE_URI : this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }

    this.camera.getPicture(options).then((imageData) => {

      let base64Image = null;
      base64Image = "data:image/jpeg;base64," + imageData;
      //add photo to the array of photos
      var name = this.config.stringGen(10);
      this.Photosarray.push({ "name": name, "src": base64Image, "desc": "" });

    }, (error) => {
      console.debug("Unable to obtain picture: " + error, "app");
      console.log(error);
    });
  }


  imageRemoved(a) {
    this.Photosarray.splice(this.Photosarray.indexOf(a.src), 1);
    //alert(this.propPhotosarray.length);
  }

  deletePhoto(name) {
    this.Photosarray.splice(this.Photosarray.indexOf(name), 1);
  }

  deletePhoto1(name) {
    this.photosdata.splice(this.photosdata.indexOf(name), 1);
  }

  deletePic(p) {
    var foundornot = false;
    for (var i = 0; i < this.deletepicarray.length; i++) {
      // alert(this.deletepicarray[i].name +"    " +p.name)
      if (this.deletepicarray[i].name == p.name) {
        foundornot = true;
        this.deletepicarray.splice(i, 1);
        break;
      }
    }

    if (foundornot == false)
      this.deletepicarray.push({ "name": p.name, "status": true });

  }

  deletePicArray() {
    for (var i = 0; i < this.deletepicarray.length; i++) {
      this.photosdata.splice(this.photosdata.indexOf(this.deletepicarray[i].name), 1);
      this.Photosarray.splice(this.Photosarray.indexOf(this.deletepicarray[i].name), 1);
    }
    this.deletepicarray = [];
  }

  changePhotoDesc(i, value) {
    //this.descdata[j] = desc;
    this.Photosarray[i].desc = value._value;
  }

  changePhotoDesc1(j, value) {
    //this.descdata[j] = desc;
    this.photosdata[j].desc = value._value;
  }

  sendImage(a, count): Promise<{}> {
    return new Promise((resolve) => {
      var url = this.config.SERVER_URL + '/upload_image.php';
      var creds = a;
      //alert(JSON.stringify(creds));
      var msg = "Please wait... Sending image to server...  ";
      this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
        try {
          //alert(JSON.stringify(ServerResponse));
          if (ServerResponse != "Error") {
            if (ServerResponse.status == "Success") {
              resolve(count);
            }
            else {
              resolve("error");
            }
          }
          else {
            resolve("error");
             this.config.presentToastError(this.config.internetConnectionErrorMsg);
          }
        }
        catch (e) {
          resolve("error");
           this.config.presentToastError(this.config.internetConnectionErrorMsg);
        }
      });
    });
  }




  senddata(i, type) {
    this.sendImage(this.Photosarray[i], i).then((Response: any) => {
      if (Response == "error") {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
      else if (Response == (this.Photosarray.length - 1)) {
        this.callserver(type);
      }
      else {
        i++;
        this.senddata(i, type);
      }
    });
  }

  AddBuyItem() {
    if (this.Photosarray.length == 0) {
      this.config.presentToastError("Please upload atleast one photo!!");
    }
    else if (this.description == "") {
      this.config.presentToastError("Please enter material name!!");
    }
    else {
      var i = 0;
      this.senddata(i, "Insert");
    }
  }

  UpdateBuyItem() {

  }
  UpdateTask() {
    if (this.description == "") {
      this.config.presentToastError("Please enter task description!");
    }
    else {
      if (this.Photosarray.length != 0) {
        var i = 0;
        this.senddata(i, "Update");
      }
      else {
        if (this.photosdata.length == 0) {
          this.config.presentToastError("Please upload atleast one photo!!");
        }
        else {
          this.callserver("Update");
        }
      }
    }
  }


  callserver(type) {
    var photosname = "";
    var descriptions = "";
    if (this.eid == 0) {
      for (var j = 0; j < this.Photosarray.length; j++) {
        if (photosname == "") {
          photosname = this.Photosarray[j].name.trim();
          descriptions = this.Photosarray[j].desc.trim();
        }
        else {
          photosname = photosname.trim() + "," + this.Photosarray[j].name.trim();
          descriptions = descriptions.trim() + "," + this.Photosarray[j].desc.trim();
        }
      }
    }
    else {
      for (var j = 0; j < this.Photosarray.length; j++) {
        if (photosname == "") {
          photosname = this.Photosarray[j].name.trim();
          descriptions = this.Photosarray[j].name.trim();
        }
        else {
          photosname = photosname.trim() + "," + this.Photosarray[j].name.trim();
          descriptions = descriptions.trim() + "," + this.Photosarray[j].desc.trim();
        }
      }
      for (var i = 0; i < this.photosdata.length; i++) {
        if (photosname == "") {
          photosname = this.photosdata[i].name.trim();
          descriptions = this.photosdata[i].desc.trim();
        }
        else {
          photosname = photosname.trim() + "," + this.photosdata[i].name.trim();
          descriptions = descriptions.trim() + "," + this.photosdata[i].desc.trim();
        }
      }
    }



    var url = this.config.SERVER_URL + '/manage_buymaterial.php';
    var creds = {
      action: type,
      created_by_id: this.userid,
      buyid: this.eid,
      property_id: this.config.BuymaterialPropid,
      task_id: this.config.BuymaterialTaskid,
      material_name: this.description,
      shope_id: JSON.stringify(this.config.BuymaterialShopes),
      photos: photosname.trim(),
      photos_desc: descriptions.trim()
    };


    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {

      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.config.presentToastSuucess(ServerResponse.remarks);
          this.navCtrl.pop();
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  getPropdata() {
    //alert(JSON.stringify(this.config.GenericPropData));
    this.propdata = this.config.GenericPropData;
    if (this.propdata.length == 0) {
      this.propdata = [];
      var url = this.config.SERVER_URL + '/manage_property.php';
      var creds = {
        action: "ReadL",
        userid: this.createdby_id,
      };
      var msg = "Please wait...";
      this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
        if (ServerResponse != "Error") {
          if (ServerResponse.status == "Success") {
            var a = JSON.parse(ServerResponse.result);
            for (var i = 0; i < a.length; i++) {
              var photossplit = a[i].photos.split(",");
              this.propdata.push({
                photo: this.config.IMAGE_URL + photossplit[0],
                name: a[i].name,
                address: a[i].address,
                propid: a[i].id,
                area: a[i].area
              })
            }
            this.menu.enable(true, 'menu2');
            this.menu.enable(false, 'menu1');
            this.menu.toggle();
            this.events.publish('setsidemenudataforprop', this.propdata);
          }
          else {
            this.config.presentToastError(ServerResponse.remarks);
          }
        }
        else {
           this.config.presentToastError(this.config.internetConnectionErrorMsg);
        }
      });
    }
    else {
      this.menu.enable(true, 'menu2');
      this.menu.enable(false, 'menu1');
      this.menu.toggle();
      this.events.publish('setsidemenudataforprop', this.propdata);
    }
  }

  getTaskData() {
    if (this.taskdata.length == 0) {
      this.taskdata = [];
      var url = this.config.SERVER_URL + '/manage_task.php';
      var creds = {
        action: "ReadByAssignedTo",
        userid: this.userid,
        property_id: this.config.BuymaterialPropid
      };
      var msg = "Please wait...";
      this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
        if (ServerResponse != "Error") {
          if (ServerResponse.status == "Success") {
            var a = JSON.parse(ServerResponse.result);
            for (var i = 0; i < a.length; i++) {
              var photossplit = a[i].photos.split(",");
              if (this.config.BuymaterialPropid == a[i].property_id) {
                var taskname = a[i].task_description.split(" ");
                if (taskname.length >= 2)
                  taskname = taskname[0] + " " + taskname[1];
                else
                  taskname = a[i].task_description;

                this.taskdata.push({
                  photo: this.config.IMAGE_URL + photossplit[0],
                  task_description: a[i].task_description,
                  task_id: a[i].id,
                  task_name: taskname
                })
              }
            }
            this.menu.enable(true, 'menu2');
            this.menu.enable(false, 'menu1');
            this.menu.toggle();
            this.events.publish('setsidemenudatafortask', this.taskdata);
          }
          else {
            this.config.presentToastError(ServerResponse.remarks);
          }
        }
        else {
           this.config.presentToastError(this.config.internetConnectionErrorMsg);
        }
      });
    }
    else {
      this.menu.enable(true, 'menu2');
      this.menu.enable(false, 'menu1');
      this.menu.toggle();
      this.events.publish('setsidemenudatafortask', this.taskdata);
    }
  }

  getShopeData() {
    if (this.shopedata.length == 0) {
      this.shopedata = [];
      var url = this.config.SERVER_URL + '/manage_shops.php';
      var creds = {
        action: "Read",
        userid: this.createdby_id
      };
      var msg = "Please wait...";
      this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
        if (ServerResponse != "Error") {
          if (ServerResponse.status == "Success") {
            this.shopedata = JSON.parse(ServerResponse.result);
            this.menu.enable(true, 'menu2');
            this.menu.enable(false, 'menu1');
            this.menu.toggle();
            this.events.publish('setsidemenudataforshope', this.shopedata);
          }
          else {
            this.config.presentToastError(ServerResponse.remarks);
          }
        }
        else {
           this.config.presentToastError(this.config.internetConnectionErrorMsg);
        }
      });
    }
    else {
      this.menu.enable(true, 'menu2');
      this.menu.enable(false, 'menu1');
      this.menu.toggle();
      this.events.publish('setsidemenudataforshope', this.shopedata);
    }
  }
}
