import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../../../providers/config';
import { LocatePage } from '../../Common/locate/locate';
import { LoadingController } from 'ionic-angular/components/loading/loading-controller';

@Component({
  selector: 'page-to-buy-list',
  templateUrl: 'to-buy-list.html'
})
export class ToBuyListPage {
  prop: any = -1;
  shop: any = -1;
  selectedpropname: any = "";


  currentuserType: any = 0;
  userid: any = 0;
  createdby_id: any;
  selectedpropid: any = 0;
  imageurl: any = "";

  propdata: any = [];
  shopedata: any = [];
  materialdata: any = [];
  materialdatacopy: any = [];

  loading: any;
  assigned_properties: any = "";

  constructor(public navCtrl: NavController, public localStorageService: Storage, public config: ConfigProvider, public loadingCtrl: LoadingController) {
  }

  ionViewDidEnter() {
    this.prop = -1;
    this.shop = -1;
    this.localStorageService.get("selectedarea").then((val) => {
      this.selectedpropname = val;
      if (val == "" || val == null)
        this.navCtrl.setRoot(LocatePage);
    });
    this.localStorageService.get("userid").then((val) => {
      this.userid = val;
      this.localStorageService.get("createdby_id").then((val) => {
        this.createdby_id = val;
        this.localStorageService.get("selectedpropid").then((val) => {
          if (val != "" && val != null)
            this.selectedpropid = val;

          this.localStorageService.get("assigned_properties").then((aval) => {
            this.assigned_properties = aval;
            if (this.config.GenericPropData.length == 0) {
              this.getPropdata();
            }
            else {
              this.propdata = this.config.GenericPropData;
              this.getShopeData();
            }
          });
        });
      });
    });
    this.localStorageService.get("usertype").then((val) => {
      this.currentuserType = val;
    });

    this.imageurl = this.config.IMAGE_URL;
  }

  getPropdata() {
    this.propdata = [];

    var url = this.config.SERVER_URL + '/manage_property.php';
    var creds = {};
    if (this.currentuserType == 2) {//for admin
      creds = {
        action: "ReadL",
        userid: this.userid,
      };
    }
    else {
      creds = {
        action: "ReadL",
        userid: this.createdby_id,
      };
    }
    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var a = JSON.parse(ServerResponse.result);
          for (var i = 0; i < a.length; i++) {
            var addornot = false;
            var as = this.assigned_properties.split(",");
            for (var j = 0; j < as.length; j++) {
              if (as[j] == a[i].id) {
                addornot = true;
                break;
              }
            }
            if (addornot) {
              var photossplit = a[i].photos.split(",");
              this.propdata.push({
                photo: this.config.IMAGE_URL + photossplit[0],
                name: a[i].name,
                address: a[i].address,
                propid: a[i].id,
                area: a[i].area
              })
            }

          }

          this.config.GenericPropData = this.propdata;
          this.getShopeData();
          //this.propdata = JSON.parse(ServerResponse.result);
          //this.propname = this.propdata[0].name;
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }


  getShopeData() {
    this.loading = this.loadingCtrl.create({
      content: "Please Wait..."
    });
    this.loading.present();
    this.shopedata = [];
    var url = this.config.SERVER_URL + '/manage_shops.php';
    var creds = {
      action: "Read",
      userid: this.createdby_id
    };
    var msg = "";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          this.shopedata = JSON.parse(ServerResponse.result);
          this.getBuyMaterialData();
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  getBuyMaterialData() {
    this.materialdata = [];

    var url = this.config.SERVER_URL + '/manage_buymaterial.php';
    var creds = {
      action: "Read",
      userid: this.userid
    };

    var msg = "";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          var resultarray = JSON.parse(ServerResponse.result);
          var a = [];
          for (var i = 0; i < resultarray.length; i++) {
            var photo = resultarray[i].photos;
            var photosplit = photo.split(",");
            if (resultarray[i].property_id == this.selectedpropid) {
              a.push({
                photo: this.config.IMAGE_URL + photosplit[0],
                buyid: resultarray[i].buyid,
                property_id: resultarray[i].property_id,
                property_name: resultarray[i].property_name,
                task_id: resultarray[i].task_id,
                task_description: resultarray[i].task_description,
                material_name: resultarray[i].material_name,
                shope_array: resultarray[i].shope_id,
                photos: resultarray[i].photos,
                photos_desc: resultarray[i].photos_desc,
                status: resultarray[i].status
              });
            }
          }

          this.materialdata = a;
          this.materialdatacopy = a;
          //alert(JSON.stringify(a));
          this.filterByShop();
          this.loading.dismiss();
        }
        else {
          this.loading.dismiss();
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
        this.loading.dismiss();
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  filterByShop() {
    this.materialdata = [];
    if (this.shop == -1) {
      this.materialdata = this.materialdatacopy;
    }
    else {
      var a = [];
      var data = this.materialdatacopy;

      for (var i = 0; i < data.length; i++) {
        var sarray = JSON.parse(data[i].shope_array);
        for (var j = 0; j < sarray.length; j++) {
          if (sarray[j].shopeid == this.shop) {
            a.push(this.materialdatacopy[i]);
          }
        }
      }

      this.materialdata = a;
    }

  }

  changeMaterialStatus(id, status) {
    var url = this.config.SERVER_URL + '/manage_buymaterial.php';
    var creds = {
      action: "ChangeStatus",
      buyid: id,
      status: status
    };

    var msg = "Please wait...";
    this.config.CallWebApi(url, JSON.stringify(creds), msg).then((ServerResponse: any) => {
      if (ServerResponse != "Error") {
        if (ServerResponse.status == "Success") {
          //var resultarray = JSON.parse(ServerResponse.result);
          this.config.presentToastSuucess(ServerResponse.remarks);
          this.getShopeData();
        }
        else {
          this.config.presentToastError(ServerResponse.remarks);
        }
      }
      else {
         this.config.presentToastError(this.config.internetConnectionErrorMsg);
      }
    });
  }

  openlocatepage() {
    this.navCtrl.push(LocatePage);
  }

}
