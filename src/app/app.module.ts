import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

//Cleaner Pages
import { ReportPage } from '../pages/Cleaner/report/report';
import { CheckPage } from '../pages/Common/check/check';
import { TaskNamePage } from '../pages/Cleaner/task-name/task-name';
import { PlanningPage } from '../pages/Cleaner/planning/planning';
import { TabsControllerCleanerPage } from '../pages/Cleaner/tabs-controller/tabs-controller';
import { LocatePage } from '../pages/Common/locate/locate';
import { WorkflowPage } from '../pages/Common/workflow/workflow';
import { EnableLoginPage } from '../pages/Common/enable-login/enable-login';
import { LoginPage } from '../pages/Common/login/login';

//Tech Pages
import { UrgentTasksPage } from '../pages/Tech/urgent-tasks/urgent-tasks';
import { ToDoPage } from '../pages/Tech/to-do/to-do';
import { TaskNamePage1 } from '../pages/Tech/task-name/task-name';
import { SubtaskPage } from '../pages/Tech/subtask/subtask';
import { BuyMaterialPage } from '../pages/Tech/buy-material/buy-material';
import { ToBuyListPage } from '../pages/Tech/to-buy-list/to-buy-list';
import { TabsControllerTechPage } from '../pages/Tech/tabs-controller/tabs-controller';

//Admin Pages
import { StatusProblemPage } from '../pages/Admin/status-problem/status-problem';
import { ReportsPage } from '../pages/Admin/reports/reports';
import { CleaningReportPage } from '../pages/Admin/cleaning-report/cleaning-report';
import { TaskNameAdminPage } from '../pages/Admin/task-name/task-name';
import { TaskDoneAdminPage } from '../pages/Admin/task-done/task-done';
import { PlanningAdminPage } from '../pages/Admin/planning/planning';
import { TabsControllerAdminPage } from '../pages/Admin/tabs-controller/tabs-controller';
import { RegisterPage } from '../pages/Admin/register/register';
import { PopoverAreaPage } from '../pages/Admin/popoverpagearea/popoverpagearea';

//Owner Pages
import { StatusPage } from '../pages/Owner/status/status';
import { ApprovePage } from '../pages/Owner/approve/approve';
import { TaskNameOwnerPage } from '../pages/Owner/task-name/task-name';
import { TabsControllerOwnerPage } from '../pages/Owner/tabs-controller/tabs-controller';
import { RequestWorkPage } from '../pages/Owner/request-work/request-work';
import { TaskDoneOwnerPage } from '../pages/Owner/task-done/task-done';


import { ConfigProvider } from '../providers/config';
import { SafePipe } from '../providers/safepipe';


import { IonicStorageModule } from '@ionic/storage';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Camera } from '@ionic-native/camera';
import { Facebook } from '@ionic-native/facebook';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Geolocation } from '@ionic-native/geolocation';

import { ComponentsModule } from '../components/components.module';
import { ProfileFivePage } from '../pages/Common/profile/profile-five';
import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [
    MyApp,
    ReportPage,
    CheckPage,
    TaskNamePage,
    PlanningPage,
    TabsControllerCleanerPage,
    LocatePage,
    WorkflowPage,
    EnableLoginPage,
    LoginPage,
    UrgentTasksPage,
    ToDoPage,
    TaskNamePage1,
    SubtaskPage,
    BuyMaterialPage,
    ToBuyListPage,
    TabsControllerTechPage,
    SafePipe,
    StatusProblemPage,
    ReportsPage,
    CleaningReportPage,
    TaskNameAdminPage,
    PlanningAdminPage,
    TabsControllerAdminPage,
    RegisterPage,
    StatusPage,
    ApprovePage,
    TaskNameOwnerPage,
    TabsControllerOwnerPage,
    RequestWorkPage,
    PopoverAreaPage,
    TaskDoneAdminPage,
    ProfileFivePage,
    TaskDoneOwnerPage
  ],
  imports: [
    HttpModule,
    HttpClientModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ReportPage,
    CheckPage,
    TaskNamePage,
    PlanningPage,
    TabsControllerCleanerPage,
    LocatePage,
    WorkflowPage,
    EnableLoginPage,
    LoginPage,
    UrgentTasksPage,
    ToDoPage,
    TaskNamePage1,
    SubtaskPage,
    BuyMaterialPage,
    ToBuyListPage,
    TabsControllerTechPage,
    StatusProblemPage,
    ReportsPage,
    CleaningReportPage,
    TaskNameAdminPage,
    PlanningAdminPage,
    TabsControllerAdminPage,
    RegisterPage,
    StatusPage,
    ApprovePage,
    TaskNameOwnerPage,
    TabsControllerOwnerPage,
    RequestWorkPage,
    PopoverAreaPage,
    TaskDoneAdminPage,
    ProfileFivePage,
    TaskDoneOwnerPage
  ],
  providers: [
    ConfigProvider,
    StatusBar,
    SplashScreen,
    SocialSharing,
    Camera,
    Facebook,
    InAppBrowser,
    Geolocation,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }