import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController, AlertController, IonicApp, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//for cleaner
import { LocatePage } from '../pages/Common/locate/locate';
import { WorkflowPage } from '../pages/Common/workflow/workflow';
import { LoginPage } from '../pages/Common/login/login';
import { TabsControllerCleanerPage } from '../pages/Cleaner/tabs-controller/tabs-controller';

//for Tech
import { TabsControllerTechPage } from '../pages/Tech/tabs-controller/tabs-controller';

//for Owner
import { TabsControllerOwnerPage } from '../pages/Owner/tabs-controller/tabs-controller';

//for Admin
import { RegisterPage } from '../pages/Admin/register/register';
import { TabsControllerAdminPage } from '../pages/Admin/tabs-controller/tabs-controller';

import { Storage } from '@ionic/storage';
import { ConfigProvider } from '../providers/config';
import { ProfileFivePage } from '../pages/Common/profile/profile-five';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
  rootPage: any;//TabsControllerPage;
  username: any = "";
  userprofileImage: any = "assets/img/profile.png";
  usertype: any = 0;

  propdata: any = [];
  proprecieveddata: any = [];

  taskdata: any = [];
  taskrecieveddata: any = [];

  shopedata: any = [];
  shoperecieveddata: any = [];

  hidepropfromrightmenu: any = false;
  hidetaskfromrightmenu: any = false;
  hideshopefromrightmenu: any = false;

  selecttext: any = "";
  hidedonebtn: any = true;


  constructor(public events: Events, public config: ConfigProvider, public alertCtrl: AlertController, public menu: MenuController, public ionicApp: IonicApp, localStorageService: Storage, public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();


      localStorageService.get("createdby_id").then((val) => {
        //alert("val " + val);
        if (val != null && val != "") {
          localStorageService.get("selectedarea").then((sval) => {

            localStorageService.get("usertype").then((val) => {
              if (val != "" && val != null) {
                this.usertype = val;
                if (val == 3)//for owner
                  this.rootPage = TabsControllerOwnerPage;
                else if (sval == "" || sval == null)
                  this.rootPage = LocatePage;
                else if (val == 2)//for admin
                  this.rootPage = TabsControllerAdminPage;
                else if (val == 4)//for tech
                  this.rootPage = TabsControllerTechPage;
                else if (val == 5)//for cleaner
                  this.rootPage = TabsControllerCleanerPage;
              }
              else {
                this.rootPage = LoginPage;
              }
            });
          });
        }
        else {
          this.rootPage = LoginPage;

        }
      });
      localStorageService.get("firstname").then((fval) => {
        if (fval != "" && fval != null) {
          localStorageService.get("lastname").then((lval) => {
            this.username = fval + " " + lval;
          });
        }
      });

      localStorageService.get("profile_pic").then((pval) => {
        //alert(this.config.IMAGE_URL + pval + ".png");
        if (pval != "" && pval != null)
          this.userprofileImage = this.config.publitioPath + this.config.publicIoThumbnail + this.config.publitioUploadProfileDirPath + "/" + pval;
      });
    });

    let ready = true;
    platform.registerBackButtonAction(() => {
      let activePortal = this.ionicApp._loadingPortal.getActive() ||
        this.ionicApp._modalPortal.getActive() ||
        this.ionicApp._toastPortal.getActive() ||
        this.ionicApp._overlayPortal.getActive();

      if (activePortal) {
        ready = false;
        activePortal.dismiss();
        activePortal.onDidDismiss(() => { ready = true; });

        console.log("handled with portal");
        return;
      }

      if (this.menu.isOpen()) {
        this.menu.close();
        console.log("closing menu");
        return;
      }

      if (!this.navCtrl.canGoBack()) {
        this.presentAlertForExitApp();
      } else {
        this.navCtrl.pop();
      }
    }, 100);

    events.subscribe('setusernameandmenu', (eventData) => {

      localStorageService.get("firstname").then((fval) => {
        if (fval != "" && fval != null) {
          localStorageService.get("lastname").then((lval) => {
            this.username = fval + " " + lval;
          });
        }
      });

      localStorageService.get("usertype").then((val) => {
        if (val != "" && val != null) {
          this.usertype = val;
        }
      });
    });

    events.subscribe('setsidemenudataforprop', (eventData) => {
      this.selecttext = "Select Property";
      this.hidedonebtn = true;
      this.hidepropfromrightmenu = false;
      this.hideshopefromrightmenu = true;
      this.hidetaskfromrightmenu = true;
      this.proprecieveddata = eventData;
      this.propdata = eventData
    });


    events.subscribe('setsidemenudatafortask', (eventData) => {
      this.selecttext = "Select Task";
      this.hidedonebtn = true;
      this.hidepropfromrightmenu = true;
      this.hideshopefromrightmenu = true;
      this.hidetaskfromrightmenu = false;
      this.taskrecieveddata = eventData;
      this.taskdata = eventData
    });

    events.subscribe('setsidemenudataforshope', (eventData) => {
      this.selecttext = "Select Location";
      this.hidedonebtn = false;
      this.hidepropfromrightmenu = true;
      this.hidetaskfromrightmenu = true;
      this.hideshopefromrightmenu = false;
      this.shoperecieveddata = eventData;
      this.shopedata = eventData
    });

  }

  goToProfile() {
    this.navCtrl.push(ProfileFivePage);
  }
  selectProperty(pid, pname) {
    this.config.BuymaterialPropid = pid;
    this.config.BuymaterialPropname = pname;
    this.menu.toggle();
  }

  getPropItems(ev: any) {
    // Reset items back to all of the items
    this.propdata = this.proprecieveddata;

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.propdata = this.propdata.filter((propdata) => {
        return (propdata.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  selectTask(tid, tname) {
    this.config.BuymaterialTaskid = tid;
    this.config.BuymaterialTaskname = tname;
    this.menu.toggle();
  }

  getTaskItems(ev: any) {
    // Reset items back to all of the items
    this.taskdata = this.taskrecieveddata;

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.taskdata = this.taskdata.filter((taskdata) => {
        return (taskdata.task_name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  closeMenuDone() {

    this.menu.toggle();
  }
  selectShope(sid, sname) {
    var foundornor = false;
    for (var a = 0; a < this.config.BuymaterialShopes.length; a++) {
      if (this.config.BuymaterialShopes[a].shopeid == sid) {
        foundornor = true;
        break;
      }
    }

    if (foundornor == false)
      this.config.BuymaterialShopes.push({ "shopeid": sid, "shopename": sname });
    else
      this.config.BuymaterialShopes.splice(this.config.BuymaterialShopes.indexOf(sid), 1);
    //this.config.BuymaterialTaskid = tid;
    //this.config.BuymaterialTaskname = tname;
    //this.menu.toggle();
  }

  getShopeItems(ev: any) {
    // Reset items back to all of the items
    this.shopedata = this.shoperecieveddata;

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.shopedata = this.shopedata.filter((shopedata) => {
        return (shopedata.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }


  //for admin
  goToRegisterProperty() {
    this.navCtrl.setRoot(RegisterPage);
  }
  goToStatusProblem() {
    this.navCtrl.setRoot(TabsControllerAdminPage, { "index": 0 });
  }
  goToReportAdmin() {
    this.navCtrl.setRoot(TabsControllerAdminPage, { "index": 1 });
  }
  goToPanningAdmin() {
    this.navCtrl.setRoot(TabsControllerAdminPage, { "index": 2 });
  }

  //for owner
  goToOwnerStatus() {
    this.navCtrl.setRoot(TabsControllerOwnerPage, { "index": 0 });
  }
  goToOwnerRequestWork() {
    this.navCtrl.setRoot(TabsControllerOwnerPage, { "index": 1 });
  }
  goToOwnerApprove() {
    this.navCtrl.setRoot(TabsControllerOwnerPage, { "index": 2 });
  }



  //for cleaner
  goToLocate(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(LocatePage);
  }
  goToReport(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(TabsControllerCleanerPage, { "index": 0 });
  }
  goToCheck(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(TabsControllerCleanerPage, { "index": 1 });
  }
  goToPlanning(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(TabsControllerCleanerPage, { "index": 2 });
  }
  goToWorkflow(params) {
    if (!params) params = {};
    this.navCtrl.push(WorkflowPage);
  }

  //for tech
  goToUrgentTasks(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(TabsControllerTechPage, { "index": 0 });
  }
  goToToDo(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(TabsControllerTechPage, { "index": 1 });
  }
  goToToBuyList(params) {
    if (!params) params = {};
    this.navCtrl.setRoot(TabsControllerTechPage, { "index": 2 });
  }



  Logout() {
    // close the menu when clicking a link from the menu
    this.menu.close();
    this.presentAlert();
  }


  presentAlertForExitApp() {
    let alert = this.alertCtrl.create({
      title: 'Exit',
      subTitle: "Do you want to close the app?",
      buttons: [
        {
          text: 'Cancel',
          handler: () => {

          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.platform.exitApp();
          }
        }
      ]
    });
    alert.present();
  }

  presentAlert() {
    let alert = this.alertCtrl.create({
      title: 'Logout',
      subTitle: "Do you want to logout?",
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            //console.log('Disagree clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.config.clearPreviousStoreddata();
            this.navCtrl.setRoot(LoginPage);
          }
        }
      ]
    });
    alert.present();
  }
}
