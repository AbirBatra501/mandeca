import { Injectable } from '@angular/core';
import { LoadingController, AlertController, ToastController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';

import 'rxjs/add/operator/timeout'

//declare var cordova: any;
@Injectable()
export class ConfigProvider {
    public enablePageActions: boolean = true;
    public uploadPhotosQoeue: any = [];
    public internetConnectionErrorMsg: any = "Something went wrong!!! Please check your internet connection"
    public usertypeforSuperAdmin: any = 1;
    public usertypeforAdmin: any = 2;
    public usertypeforOwner: any = 3;
    public usertypeforMaintainer: any = 4;
    public usertypeforCleaner: any = 5;
    public tablerowscount: any = 5;
    timeout: any = 50000;

    public GenericPropData: any = [];

    public BuymaterialPropid: any = "";
    public BuymaterialPropname: any = "";

    public BuymaterialTaskid: any = "";
    public BuymaterialTaskname: any = "";

    public BuymaterialShopes: any = [];


    //production server urls
    public hostname: string = "https://mandeca.com";//"http://localhost:8888";
    public SERVER_URL: string = this.hostname + "/api";//done
    public WT_IMAGE_URL: string = this.hostname + "/upload/files/";//done
    public IMAGE_URL: string = this.hostname + "/upload/files/Thumbnail/";//done

    publitioUploadDirPath: any = "mandeca/task/photos";
    publitioUploadProfileDirPath: any = "mandeca/userprofile";
    publitioPath = "https://media.publit.io/file/";//w_50,h_50,c_fit/mandeca/hello.png
    publicIoThumbnail = "w_70,h_70,c_fit/";
    publicIoThumbnail2 = "w_100,h_100,c_fit/";

    //production server urls
    /*     public hostname: string = "http://bluepassion.net";//"http://localhost:8888";
        public SERVER_URL: string = this.hostname + "/Maintainance/api";//done
        public IMAGE_URL: string = this.hostname + "/Maintainance/upload/images/";//done */

    constructor(public http: Http, public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public localStorageService: Storage,
        public toastCtrl: ToastController) {
    }

    CallWebApi(url, data, message): Promise<{}> {
        return new Promise((resolve) => {
            let loading = this.loadingCtrl.create({
                content: message
            });
            if (message != "")
                loading.present();
            let headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('Accept', 'application/json');
            //console.log(url);
            //console.log("data", data);
            this.http.post(url, data, {
                headers: headers
            })
                .timeout(this.timeout)
                .subscribe(
                    data => {
                        loading.dismiss();
                        try {
                            resolve(data.json());
                        }
                        catch (e) {
                            resolve();
                        }
                    },
                    err => {
                        loading.dismiss();
                        try {
                            //console.log(err);
                            resolve("Error");
                        }
                        catch (e) {
                            resolve("Error");
                        }
                    }
                );
        });
    }

    clearPreviousStoreddata() {
        this.localStorageService.set("userid", "");
        this.localStorageService.set("createdby_id", "");
        this.localStorageService.set("firstname", "");
        this.localStorageService.set("lastname", "");
        this.localStorageService.set("email", "");
        this.localStorageService.set("mobileno", "");
        this.localStorageService.set("dob", "");
        this.localStorageService.set("address", "");
        this.localStorageService.set("usertype", "");
        this.localStorageService.set("selectedpropid", "");
        this.localStorageService.set("selectedarea", "");

        this.GenericPropData = [];
        this.BuymaterialPropid = "";
        this.BuymaterialPropname = "";
        this.BuymaterialTaskid = "";
        this.BuymaterialTaskname = "";
        this.BuymaterialShopes = [];
    }

    stringGen(len) {
        var text = " ";

        var charset = "abcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < len; i++)
            text += charset.charAt(Math.floor(Math.random() * charset.length));

        return text.trim();
    }

    presentAlert(msg, title) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: msg,
            buttons: ['Dismiss'],
            enableBackdropDismiss: false
        });
        alert.present();
    }


    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom'
        });

        toast.onDidDismiss(() => {
            //console.log('Dismissed toast');
        });

        toast.present();
    }


    presentToastError(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            cssClass: "error"
        });

        toast.onDidDismiss(() => {
            //console.log('Dismissed toast');
        });

        toast.present();
    }
    loading: any;
    presentLoader(message) {
        this.loading = this.loadingCtrl.create({
            content: message
        });
        if (message != "")
            this.loading.present();
    }

    hideLoader() {
        this.loading.dismiss();
    }

    presentToastSuucess(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: 'bottom',
            cssClass: "success"
        });

        toast.onDidDismiss(() => {
            //console.log('Dismissed toast');
        });

        toast.present();
    }



}